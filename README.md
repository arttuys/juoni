﻿_Sorry, this document hasn't yet been translated to English!_

# Projektin tiedot

## Perusteet

**Juoni** on Rust-ohjelmointikielellä kirjoitettu, erityisesti *Pest.rs*-jäsentämiskirjastoa hyödyntävä tulkki. Ohjelman hyväksymä kieli on karkeasti Schemeä ja LISP-tyylisiä kieliä mukaileva kieli, mutta ei puhtaasti kummankaan periaatteita noudattava.

Tulkki on rakenteeltaan melko yksinkertainen. Jäsennin muuttaa tekstimuotoisen koodin aluksi Pest.rs:n rakenteiksi, ja siitä heti sen jälkeen ohjelman omaksi tietorakenteeksi. Tulkkausosuus siten tulkitsee ohjelman omia tietorakenteita käyden niitä vaihe vaiheelta, pala palalta lävitse. Lisäksi tulkki tarjoaa dynaamisen muuttujasidonnan esim. funktioiden käyttöön, mahdollisuuden erilaisiin näkyvyysalueisiin sekä kattavan kokelman ns. erikoismuotoja _(special forms)_ erilaisia käyttötarkoituksia varten.

## Tekniset tiedot

### Hakemistorakenne

Alla selostettu hakemistorakenne. Jotkin kansiot ja tiedostot on jätetty pois, koska ne eivät ole hyödyllisiä rakenteen ymmärtämisen kannalta ja vaihtelevat käännösympäristön mukaan

```
│   README.md (tämä tiedosto)
│   LanguageSpecification.md (kielen tekninen käyttöohje)
└───ohjelma
    │   .gitignore
    │   Cargo.lock (Cargon nyt valitut versiot paketeille)
    │   Cargo.toml (Cargon riippuvuusmääritelmät)
    │   Juoni.iml (IntellJ IDEA:n modulikansiot)
    │   
    ├───.idea    (IntelliJ IDEA:n IDE-tiedostot)
    │       .gitignore
    │       (...)
    │       
    ├───examples (Juoni-esimerkkiohjelmat)
    |       99-bottles-of-beer.juoni (99 pulloa olutta-laulu-esimerkkiohjelma)
    │       guess-my-number.juoni (Arvaa numero-esimerkkiohjelma)
    │       print-args.juoni (Tulosta argumentit-esimerkkiohjelma)
    │       repl.juoni (Read Eval Print-loop-esimerkkiohjelma)
    │       
    └───src (lähdekoodi)
        │   cli.yaml (komentoriviparametrien käännösaikainen määritelmä)
        │   main.rs (pääohjelma)
        │   
        └───interpreter (tulkin koodi)
                core.rs (perustavan tason tulkinta ja rakenteet)
                helper_macros.rs (apumakrot)
                juoni.pest (Pest.rs-kielioppi)
                mod.rs (modulirakenteen määritelmä)
                native_stdlib.rs (natiivifunktiot)
                parser.rs (Pest.rs-sidos- ja muunnosmoduli)
                scope_ops.rs (näkyvyysalueiden funktiot)
                stdlib.juoni (Juoni-kielinen standardikirjasto)
                types.rs (sisäisten tietorakenteiden määritelmät)
                world_ops.rs (ympäristön funktiot)

```


### Käännösohjeet

Ennen kuin yrität kääntää ohjelman, varmista että seuraavat vaatimukset täyttyvät:
- Testiympäristössä on toimiva Internet-yhteys (tarvitaan riippuvuuksien hakemiseen, vain ensimmäisellä kerralla tai päivityksien yhteydessä)
- Käytät joko Windows- tai (yleisesti tuettua) Linux-ympäristöä. Tämä ohje on testattu _Ubuntu 18.10_-ympäristössä, virtuaalikonetta käyttäen. Samoja ohjeita voi soveltaa todennäköisesti myös muihin Linux-ympäristöihin, esim. allekirjoittaneen käyttämään _openSUSE Leap 15.0_:iin
- Olet hakenut haluamasi version Git-versiohalinnasta valmiiksi

Juoni-tulkin voi kääntää seuraavasti

1. Varmista, että tuorein Rustin _beta_-kääntäjä on asennettuna (tämän ohjeen versio _rustc 1.34.0-beta.1 (744b374ab 2019-02-26)_). Helpoin reitti tähän on käyttää _Rustup_-päivitystyökalua. Kääntötyökalujen (rustc, cargo) tulee olla polussa.
2. Siirry `ohjelma`-kansioon, ja kirjoita `cargo build`. Cargo (riippuvuuksienhallinta) hakee tarpeelliset paketit, jä kääntää ohjelman toimintakuntoiseksi. Mikäli ongelmia ei ilmennyt, Cargo ilmoittaa onnistumisesta tekstillä `Finished dev [unoptimized + debuginfo] target(s) in <...>` tai vastaavalla.
3. Kokeile komentoa `cargo run -- examples/print-args.juoni a b c x`. Tämän pitäisi suorittaa testiohjelma `examples`-kansiosta, joka tulostaa listan argumenteista. Voit kokeilla myös muita kansiosta löytyviä ohjelmia.
4. Kokeile seuraavaksi komentoa `cargo run -- --help`. Tämän pitäisi tulostaa lista parametreista, mitä voi käyttää
5. Viimeisenä, kokeile komentoa `cargo run -- -`. Tämä avaa interaktiivisen REPL-tulkin, jossa voi testata erilaisia lausekkeita.

Mikäli nämä kaikki onnistuvat, olet onnistuneesti asentanut kehitysympäristön ja kääntänyt tulkin.

## Testaus ja käyttö

`cargo test` suorittaa joitakin yksinkertaisia yksikkötestejä kun sen suorittaa, joskin on syytä huomauttaa että nämä testit eivät tee mitenkään edistynyttä testausta. Tämä johtuu puhtaasti ajanpuutteesta. Ohjelman testaus on siten pääosin tehty käsin, suorittamalla esim. REPL-tulkilla erilaisia komentoja. 

Kielen ominaisuudet on tarkemmin määritelty `LanguageSpecification.md`-tiedostossa, jossa on selvitys sekä syntaksista että erilaisista funktioista mitä tulkissa voi käyttää, esimerkkeineen. On lisäksi varmasti hyödyllistä tutustua standardikirjaston toimintaan, joka löytyy `src/stdlib.juoni`-tiedostosta.

## Tekijätiedot

Koko projekti (poislukien ulkoiset avoimen lähdekoodin kirjastot, joita koskevat omat lisenssinsä) on lisensoitu **BSD 3-clause**-lisenssillä. Tulkin tekemiseen ei ole osallistunut muita henkilöitä kuin minä _(Arttu Ylä-Sahra)_.


