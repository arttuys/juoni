extern crate pest;
#[macro_use] extern crate pest_derive;
#[macro_use] extern crate clap;
use clap::App;
#[macro_use] mod interpreter;
use crate::interpreter::world_ops;
use crate::interpreter::core;
use crate::interpreter::native_stdlib;
use crate::interpreter::scope_ops;
use crate::interpreter::types;
use std::cell::RefCell;
use std::io::Write;
use std::rc::Rc;

use std::thread;

// Force a relatively large stack size. Perhaps a bit crude, but for the lack of time to develop better options...
const STACK_SIZE: usize = 16 * 1024 * 1024;

// Load stdlib on build
const INTEGRATED_STDLIB: &str = include_str!("interpreter/stdlib.juoni");
static SPECIAL_SYMBOLS: &'static [&'static str] = &["#t", "#f", "#undef"];

/// Initializes a baseline world and appropriate scopes; set up native functions, load standard library, and return both stdlib and current/running scope
/// # Arguments
///
/// * `load_stdlib` - If false, do not load standard library and only load native functions
pub fn initialize_baseline_world(load_stdlib: bool) -> (Rc<RefCell<types::JInterpreterWorld>>, Rc<RefCell<types::JScope>>, Rc<RefCell<types::JScope>>) {
    // Initialize a new empty world
    let world = world_ops::initialize_world();

    let base_scope = scope_ops::new_scope(&world, None);
    let mut overlay_scope = base_scope.clone();

    // Declare certain baseline symbols. This mostly ensures that these already exist when rest of the code runs, which could give a small performance advantage
    for str in SPECIAL_SYMBOLS {
        world.borrow_mut().symbol_id_for_name(str).unwrap();
    };

    // Load native stdlib functions
    {
        let mut mut_scope = base_scope.borrow_mut();
        // List functions
        mut_scope.store(&define_symbol!("#cons" => world).unwrap(), wrap_stdlib_native!(native_stdlib::cons => "cons")); // Doc OK
        mut_scope.store(&define_symbol!("#cons_hv!" => world).unwrap(), wrap_stdlib_native!(native_stdlib::cons_no_head_eval => "cons_hv!")); // Doc OK
        mut_scope.store(&define_symbol!("#list" => world).unwrap(), wrap_stdlib_native!(native_stdlib::list_eval => "list")); // Doc OK
        mut_scope.store(&define_symbol!("#head" => world).unwrap(), wrap_stdlib_native!(native_stdlib::list_head => "head")); // Doc OK
        mut_scope.store(&define_symbol!("#tail" => world).unwrap(), wrap_stdlib_native!(native_stdlib::list_tail => "tail")); // Doc OK
        // Strings and I/O
        mut_scope.store(&define_symbol!("#str" => world).unwrap(), wrap_stdlib_native!(native_stdlib::stringify_normal => "stringify_normal")); // Doc OK
        mut_scope.store(&define_symbol!("#str_annotated" => world).unwrap(), wrap_stdlib_native!(native_stdlib::stringify_annotated => "stringify_annotated")); // Doc OK
        mut_scope.store(&define_symbol!("#str_debug" => world).unwrap(), wrap_stdlib_native!(native_stdlib::stringify_debug => "stringify_debug")); // Doc OK
        mut_scope.store(&define_symbol!("#readln" => world).unwrap(), wrap_stdlib_native!(native_stdlib::readline => "readln")); // Doc OK
        mut_scope.store(&define_symbol!("#println" => world).unwrap(), wrap_stdlib_native!(native_stdlib::println => "println")); // Doc OK
        mut_scope.store(&define_symbol!("#print" => world).unwrap(), wrap_stdlib_native!(native_stdlib::print => "print")); // Doc OK
        mut_scope.store(&define_symbol!("#<>" => world).unwrap(), wrap_stdlib_native!(native_stdlib::str_concat => "str_concat")); // Doc OK
        mut_scope.store(&define_symbol!("#str_len" => world).unwrap(), wrap_stdlib_native!(native_stdlib::strlen => "str_len")); // Doc OK
        mut_scope.store(&define_symbol!("#str_substr" => world).unwrap(), wrap_stdlib_native!(native_stdlib::substring => "str_substr")); // Doc OK
        mut_scope.store(&define_symbol!("#str_to_symbol" => world).unwrap(), wrap_stdlib_native!(native_stdlib::str_to_symbol => "str_to_symbol"));
        // Quote, eval, stop
        mut_scope.store(&define_symbol!("#quote" => world).unwrap(), wrap_stdlib_native!(native_stdlib::quote => "quote")); // Doc OK
        mut_scope.store(&define_symbol!("#scope_identifier" => world).unwrap(), wrap_stdlib_native!(native_stdlib::scope_identifier => "scope_identifier"));
        mut_scope.store(&define_symbol!("#eval" => world).unwrap(), wrap_stdlib_native!(native_stdlib::eval_normal => "eval")); // Doc OK
        mut_scope.store(&define_symbol!("#eval_stdlib!" => world).unwrap(), wrap_stdlib_native!(native_stdlib::eval_stdlib => "eval_stdlib")); // Doc OK
        mut_scope.store(&define_symbol!("#eval_str" => world).unwrap(), wrap_stdlib_native!(native_stdlib::eval_str => "eval_str")); // Doc OK
        mut_scope.store(&define_symbol!("#stop!" => world).unwrap(), wrap_stdlib_native!(native_stdlib::stop => "stop")); // Doc OK
        mut_scope.store(&define_symbol!("#catch!" => world).unwrap(), wrap_stdlib_native!(native_stdlib::catch => "catch")); // Doc OK
        // Cond, and/or conditions, last
        mut_scope.store(&define_symbol!("#cond" => world).unwrap(), wrap_stdlib_native!(native_stdlib::cond => "cond")); // Doc OK
        mut_scope.store(&define_symbol!("#and" => world).unwrap(), wrap_stdlib_native!(native_stdlib::truthy_and => "and")); // Doc OK
        mut_scope.store(&define_symbol!("#or" => world).unwrap(), wrap_stdlib_native!(native_stdlib::truthy_or => "or")); // Doc OK
        mut_scope.store(&define_symbol!("#last" => world).unwrap(), wrap_stdlib_native!(native_stdlib::last => "last")); // Doc OK
        mut_scope.store(&define_symbol!("#eqv?" => world).unwrap(), wrap_stdlib_native!(native_stdlib::eqv => "eqv?")); // Doc OK
        mut_scope.store(&define_symbol!("#ineqv?" => world).unwrap(), wrap_stdlib_native!(native_stdlib::ineqv => "ineqv?")); // Doc OK

        // Essential arithmetics and comparisons
        mut_scope.store(&define_symbol!("#sum" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_plus => "numeric_plus")); // Doc OK
        mut_scope.store(&define_symbol!("#sub" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_minus => "numeric_minus")); // Doc OK
        mut_scope.store(&define_symbol!("#mul" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_mul => "numeric_mul")); // Doc OK
        mut_scope.store(&define_symbol!("#div" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_div => "numeric_div")); // Doc OK
        mut_scope.store(&define_symbol!("#<" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_smaller_than => "numeric_smaller_than")); // Doc OK
        mut_scope.store(&define_symbol!("#>" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_larger_than => "numeric_larger_than")); // Doc OK
        mut_scope.store(&define_symbol!("#<=" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_smaller_eq_than => "numeric_smaller_eq_than")); // Doc OK
        mut_scope.store(&define_symbol!("#>=" => world).unwrap(), wrap_stdlib_native!(native_stdlib::numeric_larger_eq_than => "numeric_larger_eq_than")); // Doc OK


        // (Function) definitions
        mut_scope.store(&define_symbol!("#typeof" => world).unwrap(), wrap_stdlib_native!(native_stdlib::check_type => "typeof")); // Doc OK
        mut_scope.store(&define_symbol!("#define" => world).unwrap(), wrap_stdlib_native!(native_stdlib::define => "define")); // Doc OK
        mut_scope.store(&define_symbol!("#func" => world).unwrap(), wrap_stdlib_native!(native_stdlib::func => "func")); // Doc OK
        mut_scope.store(&define_symbol!("#func*" => world).unwrap(), wrap_stdlib_native!(native_stdlib::func_vararg => "func*")); // Doc OK
        mut_scope.store(&define_symbol!("#func*!" => world).unwrap(), wrap_stdlib_native!(native_stdlib::func_stdlib_vararg => "func*!")); // Doc OK
        mut_scope.store(&define_symbol!("#func!" => world).unwrap(), wrap_stdlib_native!(native_stdlib::func_stdlib => "func!")); // Doc OK
    }

    if load_stdlib {
        // Parse stdlib, and load
        let stdlib_value = world.borrow_mut().parse_string(INTEGRATED_STDLIB, Some("stdlib"));

        if let Ok(code) = stdlib_value {
            // When running stdlib evaluation, our initial running scope and the stdlib scope are one and the same
            let val = core::evaluate_in_context(&base_scope, &base_scope, code);
            match val {
                Ok(_) => {},
                Err(e) => panic!(format!("Stdlib did not evaluate properly: {}", e))
            }
            //println!("{:?}", val);
        } else if let Err(e) = stdlib_value {
            panic!(format!("Internal error: stdlib failed to parse: {}", e));
        }

        let scope_overlay = scope_ops::new_scope(&world, Some(&base_scope));
        overlay_scope = scope_overlay;
    }

    (world, base_scope, overlay_scope)
}

/// Loads arguments into the stdlib scope, using #args
/// # Arguments
/// * `world` - World to load data in
/// * `stdlib_scope` - In which scope stdlib is
/// * `arguments` - Arguments as elements in a string vector
fn load_args(world: &mut Rc<RefCell<types::JInterpreterWorld>>, stdlib_scope: Rc<RefCell<types::JScope>>, arguments: Vec<String>) {
    // This is copied, quite verbatim, from the parser
    // First, empty tail
    let empty_tail_loc = Rc::new(RefCell::new(types::JVListNode::Empty));
    // We construct our lists one by one. We initially have nothing else but an empty list node to work with, so..
    let mut last_tail_node : Option<Rc<RefCell<types::JVListNode>>> = None;
    // Also save our initial node, we need it for longer lists
    let mut initial_node : Option<Rc<RefCell<types::JVListNode>>> = None;

    // Since list expressions usually contain subexpressions, go through them
    for argument in arguments {
        // Allocate a new position for our content, currently terminating our list in a known empty list node
        let new_list_node = Rc::new(RefCell::new(types::JVListNode::Nonempty(types::JVListNonempty {head: define_value!{default_code_location!(), modifier!{None}, content!{argument => string}}, tail: empty_tail_loc.clone()})));
        if initial_node == None {
            initial_node = Some(new_list_node.clone()); // If we do not have an initial node yet, set ours as initial. Considering our pointer is reference-counted
        }
        // If we have a tail, alter this value
        if let Some(tail_wrapper) = last_tail_node {
            // Pull in a mutable definition
            match *tail_wrapper.borrow_mut() {
                // Pattern match to the appropriate subtype
                types::JVListNode::Nonempty(ref mut obj) => {
                    obj.tail = new_list_node.clone();
                }
                _ => panic!("Reached unexpected pattern from list composition")
            }
        }

        last_tail_node = Some(new_list_node.clone());
    }

    // Finally, save our arguments in the scope as #args
    let final_list_node = initial_node.unwrap_or(empty_tail_loc);
    let arg_list = define_value!{default_code_location!(), modifier!{None}, content!{final_list_node => list}};
    let arg_symbol = define_symbol!("#args" => world).unwrap();

    stdlib_scope.borrow_mut().store(&arg_symbol, arg_list);
}

/// Initial starting point, setting up appropriate stack
fn main() {
    // Spawn a new thread with an explicit stack size
    // See: https://www.reddit.com/r/rust/comments/872fc4/how_to_increase_the_stack_size/dwa72rq
    let child = thread::Builder::new()
        .stack_size(STACK_SIZE)
        .spawn(main_2)
        .unwrap();

    // Wait for thread to join
    child.join().unwrap();
}

/// Secondary main, after a new thread has been launched with a bigger stack size
fn main_2() {

    // The YAML file is found relative to the current (source) file, similar to how modules are found (as originally quoted by Clap.rs, with annotations added)
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();

    let load_stdlib = !matches.is_present("no_stdlib");
    let (mut world, stdlib_scope, running_scope) = initialize_baseline_world(load_stdlib);

    // Check for options
    if let Some(match_vec) = matches.values_of_lossy("OPTS") {
        load_args(&mut world, stdlib_scope.clone(), match_vec);
    } else {
        load_args(&mut world, stdlib_scope.clone(), vec![]);
    }

    if let Some(fname) = matches.value_of("SOURCE") {
        if fname == "-" || fname == "" {
            repl(world, stdlib_scope, running_scope);
            return;
        }

        let parsed_code = world.borrow_mut().parse_file(fname);
        match parsed_code {
            Err(e) => {println!("Error whilst interpreting file '{}': {}", fname, e);}
            Ok(val) => {
                let res = core::evaluate_in_context(&stdlib_scope, &running_scope, val);
                match res {
                    Err(e) => {
                        println!("Interpretation/runtime error: {}", e);
                    }
                    Ok(_) => {;}
                }
            }
        }
    }
    //
    //println!("world {:?}, \nbase scope {:?}", world, base_scope);
    //println!("{:?}", world_ops::valid_symbol_name("magic"));
    //let mut world = world_ops::initialize_world();
    //world.symbol_id_for_name("magic");

    //let root_expr = world.parse_string("'(@() this @(double (triple) #lists (() () ())) ^1234 \"is magic\")", None);
    //println!("Value {},\n\n for world {:?}", root_expr.unwrap(), world);
}

/// REPL loop; read, eval, print, loop
///
/// # Arguments
/// * `world` - World to run in
/// * `stdlib_scope` - Standard library scope
/// * `running_scope` - Scope to run code in
fn repl(world: Rc<RefCell<types::JInterpreterWorld>>, stdlib_scope: Rc<RefCell<types::JScope>>, running_scope: Rc<RefCell<types::JScope>>) {
    println!("Juoni interactive mode, type '!q' to exit");

    loop {
        // Print prompt
        print!("> ");
        std::io::stdout().flush().unwrap(); // Force flush or fail

        // Take input, and trim
        let mut input = String::new();
        std::io::stdin().read_line(&mut input).unwrap(); // Read or fail, if stdin fails, something is either badly designed (file input does not do terminating commands) or we have bigger problems (I/O issues);

        let trimmed = input.trim(); // Trim extra space around input

        if trimmed.eq("!q") {
            println!("OK, exiting..");
            return;
        } else {
            let parsed_code = world.borrow_mut().parse_string(trimmed, Some("repl"));
            match parsed_code {
                Err(e) => {
                    println!("[ Parsing error: {} ]", e);
                }
                Ok(val) => {
                    let res = core::evaluate_in_context(&stdlib_scope, &running_scope, val);
                    match res {
                        Err(e) => {
                            println!("[ Interpretation/runtime error: {} ]", e);
                        }
                        Ok(res_val) => {
                            // Generate a string form
                            let string_form = native_stdlib::stringify(&mut running_scope.clone(), &res_val, native_stdlib::StringificationType::Normal);
                            println!("=> ({}) {}\n", native_stdlib::human_readable_type(&res_val.value), string_form);
                        }
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::interpreter::types;
    use crate::interpreter::world_ops;
    use crate::interpreter::scope_ops;
    use crate::interpreter::parser;

    use std::rc::Rc;
    use std::rc::Weak;
    use std::cell::RefCell;

    #[test]
    fn basic_functionality() {
        let mut world = world_ops::initialize_world();
        let symbol1 = define_symbol!("#undef" => world).unwrap();
        let symbol2 = define_symbol!("#undef" => world).unwrap();
        let symbol3 = define_symbol!("t" => world).unwrap();
        let bad_symbol = world.borrow_mut().symbol_id_for_name("(bad!)");

        assert_eq!(symbol1, symbol2);
        assert_ne!(symbol1, symbol3);
        assert_ne!(symbol2, symbol3);
        assert!(bad_symbol.is_none());

        // Initialize a new scope
        let mut scope = scope_ops::new_scope(&world, None);
        let mut inner_scope = scope_ops::new_scope(&world, Some(&scope));
        let mut inner_scope2 = scope_ops::new_scope(&world, Some(&scope));

        //let test_str = types::JValue {location: default_code_location!(), modifier: types::JVModifier::None, value: types::JValueStorageEnum::String("testing".to_owned())};
        let test_str = define_value!{default_code_location!(), modifier!{None}, content!{"testing" => string}};
        let test_str2 = define_value!{default_code_location!(), modifier!{None}, content!{"testing" => string}};
        let alt_test_str = define_value!{default_code_location!(), modifier!{None}, content!{"ineq testing" => string}};
        // Attempt to save this variable
        assert_eq!(scope.borrow_mut().store(&symbol3, test_str.clone()), None);
        // Ensure that partial equality works
        assert_eq!(scope.borrow_mut().store(&symbol3, test_str2.clone()), Some(test_str.clone()));

        // Test that scope inheritance works
        // Store to first inner scope
        inner_scope.borrow_mut().store(&symbol3, alt_test_str.clone());

        // Parent scope should reveal same contents in both cases
        {
            let scope_ref = scope.borrow();
            assert_eq!(scope_ref.has_variable_for_symbol(&symbol3, true), true);
            assert_eq!(scope_ref.has_variable_for_symbol(&symbol3, false), true);
            assert_eq!(scope_ref.get(&symbol3, true), scope_ref.get(&symbol3, false));
            // And in any case, it should be the first variable
            assert_eq!(scope_ref.get(&symbol3, true), Some(test_str.clone()))
        }

        // Inner scope should NOT be equal for the first inner scope
        {
            let scope_ref = scope.borrow();
            let inner_scope_ref = inner_scope.borrow();

            assert_eq!(inner_scope_ref.has_variable_for_symbol(&symbol3, true), true);
            assert_eq!(inner_scope_ref.has_variable_for_symbol(&symbol3, false), true);
            // Inner scope should reveal a different result to the parent scope still
            assert_ne!(inner_scope_ref.get(&symbol3, true), scope_ref.get(&symbol3, false));
            // Different as in equal to the alternative variable we placed
            assert_eq!(inner_scope_ref.get(&symbol3, true), Some(alt_test_str.clone()));
            assert_eq!(inner_scope_ref.get(&symbol3, false), Some(alt_test_str.clone()));
        }

        // Secondary inner scope should not return results for the inner scope only
        // Inner scope should NOT be equal for the first inner scope
        {
            let scope_ref = scope.borrow();
            let inner_scope_ref = inner_scope2.borrow();

            assert_eq!(inner_scope_ref.has_variable_for_symbol(&symbol3, true), true);
            assert_eq!(inner_scope_ref.has_variable_for_symbol(&symbol3, false), false);
            // Inner scope should reveal different results whererever we allow parent checking
            assert_ne!(inner_scope_ref.get(&symbol3, true), inner_scope_ref.get(&symbol3, false));
            // Different as in equal to the alternative variable we placed
            assert_eq!(inner_scope_ref.get(&symbol3, true), Some(test_str.clone()));
            assert_eq!(inner_scope_ref.get(&symbol3, false), None);
        }

    }
}