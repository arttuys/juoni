/**
    Juoni - a Scheme-like toy language written in Rust
    --
    Copyright 2019 Arttu Ylä-Sahra

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
    core.rs - Core evaluation functionality, basic language constructs are evaluated and processed in this module. For more complex functionality, see "native_stdlib.rs"
**/

use super::types;
use super::scope_ops;
use std::rc::Rc;
use std::cell::RefCell;
use super::native_stdlib;


/// Evaluates an expression in an appropriate context. This is the initial entry point for code evaluation, establishing necessary data structures
/// for `evaluate_with_state`
///
/// # Arguments
///
/// * `stdlib_scope` - Scope for the standard library; certain functionality behaves slightly differently if it would affect standard library
/// * `running_scope` - Scope for the user code
pub fn evaluate_in_context(stdlib_scope: &Rc<RefCell<types::JScope>>, running_scope: &Rc<RefCell<types::JScope>>, expr: types::JValue) -> Result<types::JValue, String> {
    // Initialize an initial evaluation data structure
    let state = types::JIState {current_scope: running_scope.clone(), stdlib_scope: stdlib_scope.clone()};

    // Pass to evaluate_with_state()
    evaluate_with_state(&state, expr)
}

/// Evaluates a given expression with a certain state, defined as a set of two scopes.
/// Returns either an evaluated native value, or an error as a human-readable string
///
/// # Arguments
///
/// * `initial_state` - State to evaluate the expression in
/// * `initial_expr` - Expression to evaluate
pub fn evaluate_with_state(initial_state: &types::JIState, initial_expr: types::JValue) -> Result<types::JValue, String> {
    // Take an independent copy of the original state
    let mut current_state = initial_state.clone();
    let mut current_expr = initial_expr.clone();

    loop {
        //println!("{:?}", current_expr);

        // Extract our location, as we need it for constructing further expressions
        let current_expr_location = current_expr.location.clone();

        // Check first if our value is verbatim.
        if current_expr.modifier == types::JVModifier::Verbatim {
            // If so, strip the outermost modifier and return as is.
            let mut cloned_val = current_expr.clone();
            cloned_val.modifier = types::JVModifier::None;
            return Ok(cloned_val)
        } else if current_expr.modifier != types::JVModifier::None {
            // Other modifiers than verbatim are not supported
            return Err("non-verbatim modifiers are not currently supported!".to_owned())
        }

        // This expression does not seem to be a special case. We can evaluate this normally
        // Next, we shall determine what we will do with our expression with a pattern match
        let result: types::JIEvalResult = match current_expr.value {
            // Strings and integers are returned as is, they do not need special processing.
            types::JValueStorageEnum::String(_) => {types::JIEvalResult::Return(current_expr)}
            types::JValueStorageEnum::Number(_) => {types::JIEvalResult::Return(current_expr)}
            // Same applies to special forms and functions alone; they are effectively opaque values. Their special meaning only comes in with a list expression
            types::JValueStorageEnum::SpecialForm(_) => {types::JIEvalResult::Return(current_expr)}
            types::JValueStorageEnum::Function(_) => {types::JIEvalResult::Return(current_expr)}
            // Symbols are resolved directly here
            types::JValueStorageEnum::Symbol(sym_val) => {
                // Retrieve a clone of our value, if available
                let value = current_state.current_scope.borrow().get(&sym_val, true);

                if let Some(val) = value {
                    // Return as is, we found it
                    types::JIEvalResult::Return(val)
                } else {
                    // Variable could not be found from any scope. Return "#undef" as a symbol, ascribed to this location
                    let symbol_data = define_symbol!("#undef" => (current_state.current_scope.borrow_mut().world)).unwrap();
                    types::JIEvalResult::Return(define_value!{current_expr_location.clone(), modifier!{None}, content!{symbol_data => symbol}})
                }
            }
            types::JValueStorageEnum::List(list_node) => {
                // This is a list. To determine appropriate action,
                let list_node_ref = &*list_node.borrow(); // Deref, and then reference into a form that pattern-matchable
                if let types::JVListNode::Nonempty(types::JVListNonempty {head, tail}) = list_node_ref {
                    // Evaluate expression found in the head of the list
                    let head_stringified = native_stdlib::stringify(&mut current_state.current_scope, &head.clone(), native_stdlib::StringificationType::Normal);
                    //println!("{}", head_stringified);
                    let head_eval_res = evaluate_with_state(&current_state, head.clone());

                    match head_eval_res {
                        Err(msg) => {return Err(msg);}
                        Ok(evaluated_head) => {
                            // We resolved to a valid result. Is this a function or a special form?
                            match evaluated_head.value {
                                types::JValueStorageEnum::Function(func) => {
                                    // This is a function.
                                    // Let's first instantiate a new scope
                                    //println!("{:?} in scope {:?}", &func, &state.current_scope);
                                    let parent_scope = match &func.interpret_in_scope {
                                        types::JScopeRef::Implicit => current_state.current_scope.clone(),
                                        types::JScopeRef::ImplicitStdlib => current_state.stdlib_scope.clone(),
                                        types::JScopeRef::FunctionParentScope(weak_scope) => {
                                            // Attempt to upgrade
                                            match weak_scope.upgrade() {
                                                Some(strong_scope) => { strong_scope}, // We still have access to the parent scope. This is OK
                                                None => {return Err("implicit scope no longer available due to weak reference having been destroyed - #self usage outside direct descendant?".to_owned());} // Parent scope has been discarded; we cannot use this
                                            }
                                        }
                                    };

                                    //println!("current scope ID: ({}), selected new parent scope: ({})", current_state.current_scope.borrow().tree_str(), parent_scope.borrow().tree_str());

                                    // Create a new inner scope that inherits from the selected parent scope
                                    let inner_scope = scope_ops::new_scope(&current_state.current_scope.borrow().world, Some(&parent_scope));

                                    // Start going through indexes and the params
                                    let mut cur_arg_index = 0;
                                    let mut current_node = tail.clone();

                                    // Varargs need a slightly different treatment, so set an offset for those
                                    let offset_for_vararg = if func.last_param_vararg {1} else {0};

                                    // If we have not reached an empty node, and we have arguments to process, place them one by one
                                    while *current_node.borrow() != types::JVListNode::Empty && func.param_symbols.len() > 0 && cur_arg_index < (func.param_symbols.len() - offset_for_vararg) { // Until we run out of arguments entirely, or have consumed enough variables to hit the vararg parameter
                                        // Irrefutable by above, but we need to clone to allow current_node to be replaced
                                        if let types::JVListNode::Nonempty(inner_nonempty) = &*current_node.clone().borrow() {
                                            // Evaluate argument
                                            let eval_res = evaluate_with_state(&current_state, inner_nonempty.head.clone());
                                            match eval_res {
                                                Err(e) => {return Err(e);} // Jump out right away, and return an error
                                                Ok(val) => {
                                                    // OK. Deposit to inner scope, and fall out
                                                    inner_scope.borrow_mut().store(func.param_symbols.get(cur_arg_index).unwrap(), val);
                                                }
                                            }
                                            current_node = inner_nonempty.tail.clone();
                                            cur_arg_index += 1;
                                        } else {
                                            unreachable!()
                                        }
                                    }

                                    // Now, next action depends on if we have varargs enabled
                                    if func.last_param_vararg {
                                        // Varargs. Check that we are on a correct argument
                                        if cur_arg_index == (func.param_symbols.len() - 1) {
                                            // Dump rest of the arguments into a list evaluation expression, evaluate that, and put them into the last argument
                                            let list_eval_sym = define_symbol!("#list" => (current_state.stdlib_scope.borrow_mut().world)).unwrap();
                                            let list_eval_sf = current_state.stdlib_scope.borrow().get(&list_eval_sym, true).unwrap();

                                            // Prepend appropriate evaluation command and construct a value
                                            let list_eval_node = nonempty_list_node!(list_eval_sf, current_node.clone());
                                            let list_eval_var = define_value!{current_expr_location.clone(), modifier!{None}, content!{list_eval_node => list}};

                                            // Evaluate it
                                            let evaled_lst_expr = evaluate_with_state(&current_state, list_eval_var);

                                            match evaled_lst_expr {
                                                Ok(val) => {inner_scope.borrow_mut().store(func.param_symbols.get(cur_arg_index).unwrap(), val);}
                                                Err(e) => {return Err(e);}
                                            }

                                            // All done!
                                        } else {
                                            return Err(format!("insufficient arguments for function call: got {}, needs at least {}", cur_arg_index, func.param_symbols.len()));
                                        }
                                    } else {
                                        // No varargs, simply check for a length mismatch
                                        if cur_arg_index != (func.param_symbols.len()) {
                                            // Length mismatch, not OK
                                            return Err(format!("insufficient arguments for function call: got {}, needs {}", cur_arg_index, func.param_symbols.len()));
                                        } else if *current_node.borrow() != types::JVListNode::Empty {
                                            return Err(format!("more parameters given than required, got more than {}", cur_arg_index));
                                        }
                                    }

                                    // Finally, save #self as a function reference with a parent scope. We weaken this to ensure that it won't induce a reference loop, a danger without GC
                                    let self_sym = define_symbol!("#self" => (current_state.stdlib_scope.borrow_mut().world)).unwrap();
                                    let mut function_clone = func.clone();
                                    function_clone.interpret_in_scope = types::JScopeRef::FunctionParentScope(Rc::downgrade(&parent_scope));

                                    // Store
                                    inner_scope.borrow_mut().store(&self_sym, define_value!{current_expr_location.clone(), modifier!{None}, content!{function_clone => function}});
                                    //println!("inner {:?}", inner_scope);

                                    // Finally, change scope and request re-evaluation with the actual code as extracted from the function object
                                    types::JIEvalResult::ReevalNewScope(func.code.borrow().clone(), inner_scope)
                                }
                                types::JValueStorageEnum::SpecialForm(sfp) => {
                                    // Right. This is a special form invocation. Call the provided native function, and return what it gives
                                    // We do not otherwise handle arguments or other aspects, allowing special behavior not possible with ordinary functions
                                    let func_pointer = &*sfp.pointer.borrow();
                                    (func_pointer)(&current_state, tail)
                                }
                                _ => {
                                    // This is not a callable list expression, making it a literal list. Rewrite to a expression with special forms that composes these into an ordinary list. This saves a bit of code reimplementation
                                    // We also ascribe the location of our expression to this transformation, as the same effect would be reached with an explicit declaration
                                    // Check for a special case. Do not allow #undef as heads in lists without explicit action, as this is a sign of an error typically
                                    if let types::JValueStorageEnum::Symbol(sym_id) = evaluated_head.value {
                                        let bad_sym_id = define_symbol!("#undef" => (current_state.stdlib_scope.borrow_mut().world)).unwrap();
                                        if sym_id == bad_sym_id {
                                            return Err(format!("potential ambiguity, evaluated list expression ({}) begins with #undef. Please use (#list ...) to construct such lists if intentional.", head_stringified).to_owned());
                                        }
                                    }

                                    // Declare appropriate symbols
                                    let list_cons_sym = define_symbol!("#cons_hv!" => (current_state.stdlib_scope.borrow_mut().world)).unwrap();
                                    let list_eval_sym = define_symbol!("#list" => (current_state.stdlib_scope.borrow_mut().world)).unwrap();

                                    // Retrieve special forms. These should be safe to unwrap unchecked, as we can assume the existence of these special forms. If they do not exist, something is seriously wrong
                                    let list_cons_sf = current_state.stdlib_scope.borrow().get(&list_cons_sym, true).unwrap();
                                    let list_eval_sf = current_state.stdlib_scope.borrow().get(&list_eval_sym, true).unwrap();

                                    // Next, let's construct an expression in form of: (#cons_hv! 'evaluated_head (#list ...tail))
                                    // #cons_hv! is a special version of cons, that does not evaluate its head again. We already have an evaluated version, we do not need more

                                    // Construct tail list node. This is, so far, relatively crude
                                    // Prepend appropriate evaluation command, and create a node
                                    let list_eval_form_node = nonempty_list_node!(list_eval_sf, tail.clone());

                                    // And put this to a variable, effectively making this: (#list ... params ...))
                                    let list_eval_var = define_value!{current_expr_location.clone(), modifier!{None}, content!{list_eval_form_node => list}};

                                    // Next, generate the cons call
                                    let list_cons_final_param = nonempty_list_node!(list_eval_var, empty_list_node!());
                                    // Take care to select the pre-evaluated version here
                                    let list_cons_mid_node = nonempty_list_node!(evaluated_head.clone(), list_cons_final_param);
                                    let list_cons_first_node = nonempty_list_node!(list_cons_sf, list_cons_mid_node);

                                    // Lift this to a variable
                                    let list_cons_var = define_value!{current_expr_location.clone(), modifier!{None}, content!{list_cons_first_node => list}};

                                    // And return for re-evaluation
                                    types::JIEvalResult::Reeval(list_cons_var)
                                }
                            }
                        }
                    }
                } else {
                    let empty_list = empty_list_node!();
                    types::JIEvalResult::Return(define_value!{current_expr_location.clone(), modifier!{None}, content!{empty_list => list}})
                }

                // Not empty. Evaluate first item
                //let head = evaluate_with_state(&current_state, list_node.bor)
            }
        };

        //println!("{:?}", &result);

        match result {
            types::JIEvalResult::Return(res) => {return Ok(res);}
            types::JIEvalResult::Reeval(rexpr) => {current_expr = rexpr; continue;}
            types::JIEvalResult::ReevalNewScope(rexpr, nscope) => {current_expr = rexpr; current_state.current_scope = nscope; continue;}
            types::JIEvalResult::Stop(msg) => {return Err(msg);}
        }
    }
}