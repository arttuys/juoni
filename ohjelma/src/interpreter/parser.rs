/**
    Juoni - a Scheme-like toy language written in Rust
    --
    Copyright 2019 Arttu Ylä-Sahra

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
    parser.rs - Interface and wrapper to Pest.rs parsing library, parsing strings into native data structures
**/

use super::types;
use std::rc::Rc;
use std::cell::RefCell;
use std::fs;
use pest::Parser;
use pest::iterators::Pair;
#[derive(Parser)]
#[grammar = "interpreter/juoni.pest"]
pub struct JuoniParser;

/// Checks if a given symbol name is valid according to the parser
/// # Arguments
/// `name` - Symbol name
pub fn is_valid_symbol(name: &str) -> bool {
    match JuoniParser::parse(Rule::symbol, name) {
        Err(_) => false,
        Ok(_) => true
    }
}

impl types::JInterpreterWorld {
    /// Parses a file, either returning a value or a string describing an error. Essentially a convenience wrapper to `parse_string`
    /// # Arguments
    /// `filename` - File to read
    pub fn parse_file(&mut self, filename: &str) -> Result<types::JValue, String> {
        let unparsed_file = fs::read_to_string(filename);

        match unparsed_file {
            Err(e) => Err(format!("Failed to read file <{}>: {}", filename, e.to_string())),
            Ok(c) => self.parse_string(&c, Some(filename))
        }
    }

    /// Parses a string, returning either a JValue valid in this world which describes the parsed code, or an error describing why parsing failed.
    /// # Arguments
    /// `code` - Code as a string to parse
    /// `location` - Location data to attach to the resulting structures
    pub fn parse_string(&mut self, code: &str, location: Option<&str>) -> Result<types::JValue, String> {
        match JuoniParser::parse(Rule::top_level_expr, code.trim()) {
            Err(e) => Err(e.to_string()),
            Ok(mut pairs) => {
                // Take the top level expression
                let enclosed_rule = pairs.next().unwrap();

                // We know for sure that the first (and only) inner rule should be an expression
                let first_inner_rule = enclosed_rule.into_inner().next().unwrap();

                //println!("{:?}", first_inner_rule);
                // Take the first expression, and recursively evaluate
                return self.interpret_and_load_expression(first_inner_rule, location)
            }
        }
    }

    /// Interprets an expression as described by a parser pair. Returns either a JValue that is valid in this world, or a string error describing what went wrong.
    /// # Arguments
    /// `pair` - Pest.rs pair to interpret, must be an expression
    /// `location` - Location to save to parts of the structure, if available
    pub fn interpret_and_load_expression(&mut self, pair: Pair<Rule>, location: Option<&str>) -> Result<types::JValue, String> {
        if pair.as_rule() != Rule::expression {
            return Err("Internal error: expected expression, but was given some other rule by parser!".to_owned())
        }

        // By default, no modifier
        let mut exp_modifier: types::JVModifier = types::JVModifier::None;

        // Initialize a location structure, defaulting if no location string was provided
        let location_data: types::JLocationData = match location {
            None => types::JLocationData {file: "unknown".to_owned(), pos: pair.as_span().start()},
            Some(file) => types::JLocationData {file: file.to_owned(), pos: pair.as_span().start()}
        };

        // Go through the possible subrules found in an expression. We should only have two of them, and an expression modifier must always come first to be considered. Ordinarily, this is ensured by the grammar.
        for inner_rule in pair.into_inner() {
            match inner_rule.as_rule() {
                // Match per inner rule
                Rule::expr_modifier => {
                    // We found an expression modifier. Apply this to our variable
                    // Match by string value
                    exp_modifier = match inner_rule.as_str() {
                        "'" => types::JVModifier::Verbatim,
                        "`" => types::JVModifier::Quote,
                        "^" => types::JVModifier::Unquote,
                        "@" => types::JVModifier::UnquoteDestructure,
                        _ => panic!("Internal error: Impossible expression modifier found")
                    }
                }
                Rule::symbol => {
                    // Found a symbol. Insert this to our symbol table or error
                    let symbol_id = self.symbol_id_for_name(inner_rule.as_str());
                    return match symbol_id {
                        Some(symbol_identifier) => {
                            Ok(types::JValue {location: location_data.clone(), modifier: exp_modifier, value: types::JValueStorageEnum::Symbol(symbol_identifier)})
                        }
                        None => {
                            Err(format!("error at pos {} in <{}>: symbol {} is not validly formed or could not be inserted due to an internal error", location_data.pos, location_data.file, inner_rule.as_str()).to_owned())
                        }
                    };
                }
                Rule::number => {
                    // Our grammar forces numbers to be relatively well-formed - so to save time, we can safely use Rust's integrated parsing functionality
                    return match inner_rule.as_str().parse::<types::JVNumber>() {
                        Ok(number) => Ok(types::JValue {location: location_data.clone(), modifier: exp_modifier, value: types::JValueStorageEnum::Number(number)}),
                        Err(e) => Err(format!("error {} at pos {} in <{}>: malformed number '{}'", e, location_data.pos, location_data.file, inner_rule.as_str()).to_owned())
                    }
                }
                Rule::string_inner_dq => {
                    // Double quoted string
                    // Allocate a new storage location for our string and save
                    return Ok(types::JValue {location: location_data.clone(), modifier: exp_modifier, value: types::JValueStorageEnum::String(inner_rule.as_str().to_owned())})
                }
                Rule::string_inner_bq => {
                    // Bar quoted string. Essentially equivalent to a double quoted string, except by syntax
                    return Ok(types::JValue {location: location_data.clone(), modifier: exp_modifier, value: types::JValueStorageEnum::String(inner_rule.as_str().to_owned())})
                }
                Rule::list => {
                    // A list. This is a bit more complicated to parse, as we need to recursively interpret substructures

                    // First, allocate our empty tail, where our list will always end
                    let empty_tail_loc = Rc::new(RefCell::new(types::JVListNode::Empty));
                    // We construct our lists one by one. We initially have nothing else but an empty list node to work with, so..
                    let mut last_tail_node : Option<Rc<RefCell<types::JVListNode>>> = None;
                    // Also save our initial node, we need it for longer lists
                    let mut initial_node : Option<Rc<RefCell<types::JVListNode>>> = None;

                    // Since list expressions usually contain subexpressions, go through them
                    for list_inner in inner_rule.into_inner() {
                        // Evaluate inner expression
                        let inner_res = self.interpret_and_load_expression(list_inner, location);
                        // Check what we got
                        match inner_res {
                            Err(_) => return inner_res,
                            Ok(value) => {
                                // Allocate a new position for our content, currently terminating our list in a known empty list node
                                let new_list_node = Rc::new(RefCell::new(types::JVListNode::Nonempty(types::JVListNonempty {head: value, tail: empty_tail_loc.clone()})));
                                if initial_node == None {
                                    initial_node = Some(new_list_node.clone()); // If we do not have an initial node yet, set ours as initial. Considering our pointer is reference-counted
                                }
                                // If we have a tail, alter this value
                                if let Some(tail_wrapper) = last_tail_node {
                                    // Pull in a mutable definition
                                        match *tail_wrapper.borrow_mut() {
                                            // Pattern match to the appropriate subtype
                                            types::JVListNode::Nonempty(ref mut obj) => {
                                                obj.tail = new_list_node.clone();
                                            }
                                            _ => panic!("Reached unexpected pattern from list composition")
                                        }
                                }

                                last_tail_node = Some(new_list_node.clone());
                            }
                        }
                    }

                    // Finally, return our list expression as the initial node - or if we have an empty list, a reference to the empty list
                    let final_list_node = initial_node.unwrap_or(empty_tail_loc);
                    return Ok(types::JValue {location: location_data.clone(), modifier: exp_modifier, value: types::JValueStorageEnum::List(final_list_node)})
                }
                _ => panic!("Internal error: Impossible subexpression type found")
            }
        }

        Err("internal error: was able to return without catching a valid expression".to_owned())
    }
}