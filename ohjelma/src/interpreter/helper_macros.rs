/**
    Juoni - a Scheme-like toy language written in Rust
    --
    Copyright 2019 Arttu Ylä-Sahra

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
    helper_macros.rs - Helper macros to reduce the amount of repetitive boilerplate required for common operations
**/

/// Helper macro for empty code locations, e.g native functionality or otherwise undefinable location
#[macro_export]
macro_rules! default_code_location {
    () => {types::JLocationData {file: "undefined".to_owned(), pos: 0}}
}

/// Helper macro for conveniently defining and retrieving symbols for names
#[macro_export]
macro_rules! define_symbol {
    ($e:expr => $w:expr) => {$w.borrow_mut().symbol_id_for_name($e)}
}

/// Define value, create a JValue structure
#[macro_export]
macro_rules! define_value {
    {$loc:expr, $modif:expr, $value:expr} => {types::JValue {location: $loc, modifier: $modif, value: $value}}
}

/// Modifiers; so far, nothing else than None has been required
#[macro_export]
macro_rules! modifier {
    {None} => {types::JVModifier::None}
}

/// Content, creating appropriately typed value storage structure
#[macro_export]
macro_rules! content {
    {$e:expr => string} => {types::JValueStorageEnum::String($e.to_owned())};
    {$e:expr => symbol} => {types::JValueStorageEnum::Symbol($e)};
    {$e:expr => number} => {types::JValueStorageEnum::Number($e)};
    {$e:expr => list} => {types::JValueStorageEnum::List($e)};
    {$e:expr => function} => {types::JValueStorageEnum::Function($e)};
}

/// Empty list node
macro_rules! empty_list_node {
    () => {{Rc::new(RefCell::new(types::JVListNode::Empty))}}
}

/// Nonempty list node, with a head and a tail
macro_rules! nonempty_list_node {
    ($head:expr, $tail:expr) => {Rc::new(RefCell::new(types::JVListNode::Nonempty(types::JVListNonempty {head: $head, tail: $tail})));}
}

/// A special form, wrapping a native function into a JValue object
macro_rules! wrap_stdlib_native {
    ($id:path => $name:expr) => {types::JValue {location: types::JLocationData {file: "native".to_owned(), pos: 0}, modifier: types::JVModifier::None, value: types::JValueStorageEnum::SpecialForm(types::JSpecialForm {name: $name.to_owned(), pointer: Rc::new(RefCell::new($id))})}}
}

/// Evaluate or fail; attempt to evaluate a JValue, and short-circuit to a return if this fails
macro_rules! evaluate_or_fail {
    ($val:ident in $state:ident as $res:ident) => {
        match core::evaluate_with_state($state, $val) {
            Err(e) => { return types::JIEvalResult::Stop(e); }
            Ok(jv) => { $res = jv; }
        }
    }
}