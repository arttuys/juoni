/**
    Juoni - a Scheme-like toy language written in Rust
    --
    Copyright 2019 Arttu Ylä-Sahra

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

extern crate rand;

use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

use super::types;
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

/// Initializes a new scope, optionally taking a reference to a parent scope to be ingested to the new scope. A reference to the world is always required.
/// # Arguments
/// `world` - Reference to the world related to this scope
/// `parent_scope` - If one exists, a parent scope to reference to
///
pub fn new_scope(world: &Rc<RefCell<types::JInterpreterWorld>>, parent_scope: Option<&Rc<RefCell<types::JScope>>>) -> Rc<RefCell<types::JScope>> {
    let parent: Option<Rc<RefCell<types::JScope>>> = match parent_scope {
        None => None,
        Some(reference) => Some(reference.clone()) // Due to interior mutability, we are able to make this clone even with an apparently read-only pointer
    };

    // Initialize a new random scope identifier for this scope
    // Example per: https://rust-lang-nursery.github.io/rust-cookbook/algorithms/randomness.html
    let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(24)
        .collect();

    let scope = Rc::new(RefCell::new(types::JScope {
        world: world.clone(),
        parent_scope: parent,
        variable_bindings: HashMap::new(),
        rand_id: rand_string
    }));

    //println!("new scope {}", scope.borrow().tree_str());

    scope
}

impl types::JScope {
    /// Checks if we have a definition for this symbol, either directly or by inheritance from our parent scope
    /// # Arguments
    /// * `symbol_id` - Symbol identifier to check
    /// * `check_parent` - Check parent recursively. Ensure that no mutable borrows are held to the parent scope if recursive checking is enabled
    pub fn has_variable_for_symbol(&self, symbol_id: &types::JVSymbolIdentifier, check_parent: bool) -> bool {
        // Base case, contains key
        if self.variable_bindings.contains_key(symbol_id) {
            return true;
        }

        // If we desire to check our parent scope, and we actually have one
        if check_parent && self.parent_scope.is_some() {
            // Attempt to borrow the parent scope
            return self.parent_scope.as_ref().unwrap().borrow().has_variable_for_symbol(symbol_id, true)
        }

        // No match, return false
        return false
    }

    /// Attempts to retrieve a clone of the value keyed to a certain symbol identifier.
    /// # Arguments
    /// * `symbol_id` - Symbol identifier for the data to look up
    /// * `from_parent` - If true, `get` is implicitly called for the parent scope as well, returning its value if one cannot be found from this scope. Do note that mutable borrows should not be held to parent scopes
    pub fn get(&self, symbol_id: &types::JVSymbolIdentifier, from_parent: bool) -> Option<types::JValue> {
        //println!("get {} from {}", symbol_id, self.tree_str());

        // Base case, we have this
        if let Some(value) = self.variable_bindings.get(symbol_id).and_then(|rf| Some(rf.clone())) {
            return Some(value)
        } else if from_parent {
            // Check from parent if appropriate
            return self.parent_scope.as_ref().and_then(|inner_rc| inner_rc.borrow().get(symbol_id, true))
        } else {
            // Nope? None then
            None
        }
    }

    /// Stores a JValue, returning old value if any.
    /// # Arguments
    /// * `symbol_id` - Symbol identifier for data to tore
    /// * `value` - JValue to store
    pub fn store(&mut self, symbol_id: &types::JVSymbolIdentifier, value: types::JValue) -> Option<types::JValue> {
        //println!("store to {} in scope {}", symbol_id, self.tree_str());

        return self.variable_bindings.insert(symbol_id.clone(), value)
    }

    /// Return the scope tree string for the current scope. This is useful for determining the exact parentage of our scope
    /// Scope tree strings are in form of <scope identifier>->...-><scope identifier>->*, building a chain towards the scope with no parent
    pub fn tree_str(&self) -> String {
        // Recursively request the string from our parent, or mark with a * to show the end of chain
        let further_str = match &self.parent_scope {
            None => "*".to_owned(),
            Some(parent) => parent.borrow().tree_str()
        };
        return format!("{}->{}", self.rand_id.clone(), further_str)
    }
}