/**
    Juoni - a Scheme-like toy language written in Rust
    --
    Copyright 2019 Arttu Ylä-Sahra

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
    native_stdlib.rs - Native standard functions
**/

/**
    # Observations about documentation and structure of this module

    Most functions in this module are of form:
    ```
    function(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult
    ```

    These functions are the form as required by JSpecialForm, the type for native functions and special forms as to separate them from normal Juoni functions.
    Standard arguments are defined as follows:

    * `state` - Reference to the current state, with current scopes as used by `core` for interpretation
    * `params` - Reference to the parameter list, effectively a tail of the list with a function or a special form as its head.

    Nonstandard arguments are separately documented.
**/

use super::types;
use super::core;
use super::world_ops;
use std::io;
use std::io::Write;
use std::rc::Rc;
use std::cell::RefCell;

/// Type inspection: return a symbol defining the precise type - or if not possible, returns `#undef`
pub fn check_type(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    let location; // Location variable

    let t_str = if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        if let types::JVListNode::Nonempty(_) = &*(nonempty.tail).borrow() {
            return types::JIEvalResult::Stop("typeof: too many arguments".to_owned());
        }
        let mut evaled_param : types::JValue;
        let unevaled_param = nonempty.head.clone();

        // Evaluate our argument
        evaluate_or_fail!(unevaled_param in state as evaled_param);

        // Mark location as the location of evaluated result, as effectively type derives from that
        location = evaled_param.location.clone();

        human_readable_type(&evaled_param.value) // Human readable type
    } else {
        location = default_code_location!(); // We do not have a valid location of any kind
        "#undef".to_owned() // Undef, no parameter
    };

    // Construct a symbol, and return
    let symbol_data = define_symbol!(&t_str => (state.current_scope.borrow_mut().world)).unwrap();
    types::JIEvalResult::Return(define_value! {location, modifier!{None}, content!{symbol_data => symbol}})
}

/// Convert a value to a human readable type name
/// # Arguments
/// * `value` - Reference to a value to convert
pub fn human_readable_type(value: &types::JValueStorageEnum) -> types::JVString {
    match value {
        types::JValueStorageEnum::List(_) => "list",
        types::JValueStorageEnum::Symbol(_) => "symbol",
        types::JValueStorageEnum::Number(_) => "number",
        types::JValueStorageEnum::String(_) => "string",
        types::JValueStorageEnum::Function(_) => "function",
        types::JValueStorageEnum::SpecialForm(_) => "special_form"
    }.to_owned()
}

/// Type of stringification
pub enum StringificationType {
    /// Normal, least amount of annotations as possible
    Normal,
    /// Annotated, some annotations but not as comprehensively as Debug
    Annotated,
    /// Debug, exposes full JValue structure
    Debug
}

/// Stringification: convert a value to a human readable form. Annotated display can also be activated. There is also a separate debug functionality
pub fn stringify(scope: &mut Rc<RefCell<types::JScope>>, val: &types::JValue, t: StringificationType) -> types::JVString {
    match t {
        crate::interpreter::native_stdlib::StringificationType::Normal => {
            match &val.value {
                types::JValueStorageEnum::Number(num) => format!("{}", num),
                types::JValueStorageEnum::String(str) => format!("{}", str),
                types::JValueStorageEnum::Symbol(sym_id) => format!("{}", scope.borrow().world.borrow().name_for_id(&sym_id).unwrap()),
                types::JValueStorageEnum::SpecialForm(sf) => format!("<special form:{}>", sf.name),
                _ => format!("{}", val.value)
            }
        }
        crate::interpreter::native_stdlib::StringificationType::Annotated => format!("{}", val.value),
        crate::interpreter::native_stdlib::StringificationType::Debug => format!("{:?}", val)
    }
}

/// Single argument function wrapper; all native functions have a fair bit of handling boilerplate, so it is useful to have a wrapper for common cases
///
/// # Nonstandard arguments
/// * `fun_name` - Which human-readable name this function has?
/// * `preeval` - Pre-evaluate our argument before passing it onwards?
/// * `op` - Boxed closure, which takes a scope and the value to process
pub fn single_value_func_wrapper(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, fun_name: &str, preeval: bool, mut op: Box<FnMut(&mut Rc<RefCell<types::JScope>>, types::JValue) -> types::JIEvalResult>) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        if let types::JVListNode::Nonempty(_) = &*(nonempty.tail).borrow() {
            return types::JIEvalResult::Stop(format!("{}: too many arguments", fun_name));
        }

        if !preeval {
            // No-pre-evaluation required
            op(&mut state.current_scope.clone(), nonempty.head.clone())
        } else {
            // Pre-evaluate
            let unevaled = nonempty.head.clone();
            let mut evaled_head;
            evaluate_or_fail!(unevaled in state as evaled_head);
            op(&mut state.current_scope.clone(), evaled_head)
        }
    } else {
        // No parameters
        types::JIEvalResult::Stop(format!("{}: no argument provided, needs 1", fun_name))
    }
}

/// Print function; prints to standard out, implicitly stringifying its value
/// This function is to be used with single-value function wrapper
///
/// # Nonstandard arguments
/// * `newline` - If true, print a newline after the value
pub fn print_val(scope: &mut Rc<RefCell<types::JScope>>, val: types::JValue, newline: bool) -> types::JIEvalResult {
    // Convert to string
    let str = stringify(scope, &val, StringificationType::Normal);

    // Print with or without newline
    if newline {
        println!("{}", str);
    } else {
        print!("{}", str);
    }

    // Flush IO, panicking if something fails
    io::stdout().flush().unwrap();

    // Return original value
    return types::JIEvalResult::Return(val);
}

/// Returns the current scope tree as a string
pub fn scope_identifier(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    if *(params).borrow() != types::JVListNode::Empty {
        return types::JIEvalResult::Stop("scope_identifier: no arguments should be provided".to_owned());
    }

    let scope_str = state.current_scope.borrow().tree_str();

    types::JIEvalResult::Return(define_value! {default_code_location!(), modifier!{None}, content!{scope_str => string}})
}

/// Print without newline, wrapper to single-value function
pub fn print(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "print", true, Box::new(|scope, value| {print_val(scope, value, false)}))
}

/// Print with newline, wrapper
pub fn println(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "print", true, Box::new(|scope, value| {print_val(scope, value, true)}))
}

/// Stringify in normal form, wrapper
pub fn stringify_normal(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "str", true, Box::new(|scope, value| {let str = stringify(scope, &value, StringificationType::Normal); types::JIEvalResult::Return(define_value! {value.location.clone(), modifier!{None}, content!{str => string}})}))
}

/// Stringify in annotated form, wrapper
pub fn stringify_annotated(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "str_annotate", true, Box::new(|scope, value| {let str = stringify(scope, &value, StringificationType::Annotated); types::JIEvalResult::Return(define_value! {value.location.clone(), modifier!{None}, content!{str => string}})}))
}

/// Stringify in debug form, wrapper
pub fn stringify_debug(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "str_debug", true, Box::new(|scope, value| {let str = stringify(scope, &value, StringificationType::Debug); types::JIEvalResult::Return(define_value! {value.location.clone(), modifier!{None}, content!{str => string}})}))
}

/// Converts a string to a symbol, or errors if not possible
pub fn str_to_symbol(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "str_to_symbol", true, Box::new(|scope: &mut Rc<RefCell<types::JScope>>, value: types::JValue| {
        // Take location of the parameter, as we need it
        let location = value.location.clone();
        match value.value {
            types::JValueStorageEnum::String(str) => {
                let world = scope.borrow_mut().world.clone();

                // If not a valid symbol, error
                if !world_ops::valid_symbol_name(&str) {
                    return types::JIEvalResult::Stop("str_to_symbol: not a grammatically valid name for a symbol".to_owned());
                }

                // This again should be safe to do - it is more desirable to panic if we truly run out of symbol IDs, the only other possible option apart from having a grammatically invalid name
                let symbol_id = world.borrow_mut().symbol_id_for_name(&str).unwrap();

                // Return a symbol, with location derived from value
                types::JIEvalResult::Return(define_value! {location, modifier!{None}, content!{symbol_id => symbol}})
            }
            _ => types::JIEvalResult::Stop("str_to_symbol: not a string".to_owned())
        }
    }))
}

/// Return length of a string in characters, or error if not a string
pub fn strlen(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "str_len", true, Box::new(|_, value: types::JValue| {
        match &value.value {
            types::JValueStorageEnum::String(str) => {
                types::JIEvalResult::Return(define_value! {value.location, modifier!{None}, content!{str.chars().count() as i64 => number}})
            }
            _ => types::JIEvalResult::Stop("str_len: not a string".to_owned())
        }
    }))
}

/// Substring from a native string, or an error if not a string
pub fn substring(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        let unevaled_str_head = nonempty.head.clone();
        // Save location of the unevaled head, as this indicates the location of our call
        let unevaled_location = unevaled_str_head.location.clone();

        let mut evaled_str_head;
        evaluate_or_fail!(unevaled_str_head in state as evaled_str_head);
        match &evaled_str_head.value {
            // First nesting, extract string
            types::JValueStorageEnum::String(str) => {
                if let types::JVListNode::Nonempty(inner_nonempty) = &*nonempty.tail.borrow() {
                    let unevaled_start_index = inner_nonempty.head.clone();
                    let mut evaled_start_index : types::JValue;
                    evaluate_or_fail!(unevaled_start_index in state as evaled_start_index);

                    // Extract start index
                    let start_index : i64 = match evaled_start_index.value {
                        types::JValueStorageEnum::Number(num) => {
                            if num < 0 {
                                return types::JIEvalResult::Stop("str_substr: starting index less than zero".to_owned());
                            }
                            num
                        },
                        _ => {return types::JIEvalResult::Stop("str_substr: second argument not a number".to_owned());}
                    };

                    // If available, take a count of characters
                    let maybe_count : Option<i64> = if let types::JVListNode::Nonempty(innermost_nonempty) = &*inner_nonempty.tail.borrow() {
                        let unevaled_count = innermost_nonempty.head.clone();
                        let evaled_count;

                        evaluate_or_fail!(unevaled_count in state as evaled_count);
                        match evaled_count.value {
                            types::JValueStorageEnum::Number(num) => {
                                if num < 0 {
                                    return types::JIEvalResult::Stop("str_substr: count less than zero".to_owned());
                                }
                                Some(num)
                            }
                            _ => {return types::JIEvalResult::Stop("str_substr: second argument not a number".to_owned());}
                        }
                    } else {
                        None
                    };

                    // Create a substring using an iterator; skip with the starting index, and take maximally count
                    let substring : String = match maybe_count {
                        Some(count) => str.chars().skip(start_index as usize).take(count as usize).collect(),
                        None => str.chars().skip(start_index as usize).collect()
                    };

                    // Return
                    types::JIEvalResult::Return(define_value! {unevaled_location, modifier!{None}, content!{substring => string}})
                } else {
                    types::JIEvalResult::Stop("str_substr: found 1 argument, needs 2".to_owned())
                }
            }
            _ => {
                return types::JIEvalResult::Stop("str_substr: first argument must be a string".to_owned())
            }
        }
    } else {
        types::JIEvalResult::Stop("str_substr: found 0 arguments, needs 2".to_owned())
    }
}

/// Stringifies a value, and returns a stop
pub fn stop(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "stop", true, Box::new(|scope, value| {types::JIEvalResult::Stop(stringify(scope, &value, StringificationType::Normal))}))
}

/// Eval a string, first parsing and then asking for a re-evaluation if valid
pub fn eval_str(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "eval", true, Box::new(|scope, value| {
        let eval_str = stringify(scope, &value, StringificationType::Normal);
        let parsed_datatype = scope.borrow_mut().world.borrow_mut().parse_string(&eval_str, Some("eval"));

        match parsed_datatype {
            Ok(val) => types::JIEvalResult::Reeval(val),
            Err(e) => types::JIEvalResult::Stop(format!("Evaluation error: {}", e))
        }
    }))
}

/// Catch; this is the only provision for stopping Stop results from propagating upwards, effectively allowing catching errors.
pub fn catch(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        // Take unevaled location of ours
        let unevaled_location = nonempty.head.location.clone();

        if *nonempty.tail.borrow() != types::JVListNode::Empty {
            return types::JIEvalResult::Stop("catch: must not have more than one argument".to_owned());
        }

        // Evaluate, this time not using the macro. We need to handle the result ourself
        let (res_symbol, res_value) = match core::evaluate_with_state(state, nonempty.head.clone()) {
            Ok(val) => ("ok".to_owned(), val),
            Err(e) => ("error".to_owned(), define_value! {unevaled_location.clone(), modifier!{None}, content!{e => string}})
        };

        let symbol_data = define_symbol!(&res_symbol => (state.current_scope.borrow_mut().world)).unwrap();
        let symbol_val = define_value! {unevaled_location.clone(), modifier!{None}, content!{symbol_data => symbol}};
        // Define a list
        let list_node = nonempty_list_node!(symbol_val, nonempty_list_node!(res_value, empty_list_node!()));
        let list_val = define_value! {unevaled_location.clone(), modifier!{None}, content!{list_node => list}};
        types::JIEvalResult::Return(list_val)
    } else {
        types::JIEvalResult::Stop("catch: must have one argument".to_owned())
    }
}

/// Quote is perhaps simplest to implement, return as is without pre-eval. Quite equivalent to a verbatim modifier
pub fn quote(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "quote", false, Box::new(|_scope, value| {types::JIEvalResult::Return(value)}))
}

/// Eval is simple to implement; with pre-evaluated data, ask for a re-eval
pub fn eval_normal(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    single_value_func_wrapper(state, params, "eval", true, Box::new(|_scope, value| {types::JIEvalResult::Reeval(value)}))
}

/// Stdlib evaluation is quite similar - evaluate in changed scope
pub fn eval_stdlib(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    let stdlib_scope = state.stdlib_scope.clone();
    single_value_func_wrapper(state, params, "eval_stdlib!", true, Box::new(move |_scope, value| {types::JIEvalResult::ReevalNewScope(value, stdlib_scope.clone())}))
}

/// Last; discard all but last expression's results, re-evaling the last one allowing no stack growth
pub fn last(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        let mut current_node = nonempty.clone();

        loop {
            match &*current_node.clone().tail.borrow() {
                types::JVListNode::Empty => return types::JIEvalResult::Reeval(current_node.head.clone()),
                types::JVListNode::Nonempty(inner_nonempty) => {
                    let unevaled_head = current_node.head.clone();
                    let mut _evaled_head; // Not used, required by macro

                    evaluate_or_fail!(unevaled_head in state as _evaled_head);

                    current_node = inner_nonempty.clone()
                }
            }
        }
    } else {
        types::JIEvalResult::Stop("last: must have at least one argument".to_owned())
    }
}

/// Define; take a symbol of appropriate kind, and declare a variable in current scope as evaluated
/// This has a special property that any names starting with a # cannot be declared outside the stdlib scope; this will return '#stdlib-define-error'
pub fn define(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        // Pull in the first parameter
        let symbol_raw = nonempty.head.clone();
        let mut symbol : types::JValue;
        // Evaluate the symbol identifier normally
        evaluate_or_fail!(symbol_raw in state as symbol);
        if let types::JVListNode::Nonempty(inner_nonempty) = &*(nonempty.tail).borrow() {
            // Take the raw location of the definition
            let definition_location = inner_nonempty.head.location.clone();
            // Check if we have too many arguments?
            if let types::JVListNode::Nonempty(_) = &*(inner_nonempty.tail).borrow() {
                return types::JIEvalResult::Stop("define: too many arguments".to_owned());
            }
            // Next, before doing an expensive evaluation, check that we actually have a symbol name
            let extr_sym_id = match symbol.value {
                types::JValueStorageEnum::Symbol(sym_id) => {
                    // Retrieve the name. This is safe to do, as if we have a symbol identifier, we can assume it is also defined
                    let name = state.current_scope.borrow().world.borrow().name_for_id(&sym_id).unwrap();
                    if name == "#undef" {
                        return types::JIEvalResult::Stop("#define: attempted to redefine #undef, possible error?".to_owned());
                    } else if name.starts_with("#") && (state.current_scope != state.stdlib_scope) {
                        // Not OK, this is a protected identifier and we are not in stdlib
                        let symbol_data = define_symbol!("#stdlib_define_error" => (state.current_scope.borrow_mut().world)).unwrap();
                        return types::JIEvalResult::Return(define_value! {definition_location, modifier!{None}, content!{symbol_data => symbol}});
                    }

                    sym_id
                },
                _ => {return types::JIEvalResult::Stop("define: name is not a symbol".to_owned());}

            };
            // Symbol is valid? OK. Evaluate, update location to the position of the definition, and save
            let val_raw = inner_nonempty.head.clone();
            let mut val : types::JValue;
            evaluate_or_fail!(val_raw in state as val);

            // Update location
            val.location = definition_location;

            // Save
            state.current_scope.borrow_mut().store(&extr_sym_id, val.clone());

            // Return the evaluated value
            types::JIEvalResult::Return(val)
        } else {
            types::JIEvalResult::Stop("define: needs 2 arguments to define, found 1".to_owned())
        }
    } else {
        types::JIEvalResult::Stop("define: needs 2 arguments to define, found 0".to_owned())
    }
}

/// Simple value equality test - all must be equal
pub fn eqv(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_bool_fn(state, params, "eqv?", Box::new(move |a,b| Ok((b.clone(), a == b))), true)
}

/// Simple variadic inequality test - if values are inequal even on one part, return true
pub fn ineqv(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_bool_fn(state, params, "ineqv?", Box::new(move |a,b| Ok((b.clone(), a != b))), false) // Even one mismatch is enough for inequality
}

/// Numeric smaller than
pub fn numeric_smaller_than(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_bool_fn(state, params, "<", variadic_numeric_bool_wrapper(Box::new(|a,b| Ok((b, a<b)))), true)
}

/// Numeric smaller or equal than
pub fn numeric_smaller_eq_than(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_bool_fn(state, params, "<=", variadic_numeric_bool_wrapper(Box::new(|a,b| Ok((b, a<=b)))), true)
}

/// Numeric larger than
pub fn numeric_larger_than(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_bool_fn(state, params, ">", variadic_numeric_bool_wrapper(Box::new(|a,b| Ok((b, a>b)))), true)
}

/// Numeric larger or equal than
pub fn numeric_larger_eq_than(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_bool_fn(state, params, ">=", variadic_numeric_bool_wrapper(Box::new(|a,b| Ok((b, a>=b)))), true)
}

/// Variadic numeric bool function wrapper; adds matching guards to number-only functions to assure type safety, and otherwise just passes values
/// This is used to convert numeric-only functions to standard variadic functions, whilst ensuring type safety.
///
/// This function is to be used with reducing boolean functions, and should not be confused with the common reducing variant
pub fn variadic_numeric_bool_wrapper(num_op: Box<Fn(types::JVNumber, types::JVNumber) -> Result<(types::JVNumber, bool), String>>) -> Box<Fn(types::JValue, types::JValue) -> Result<(types::JValue, bool), String>> {
    Box::new(move |a: types::JValue, b: types::JValue| -> Result<(types::JValue, bool), String> {
        if let types::JValueStorageEnum::Number(a_num) = a.value {
            if let types::JValueStorageEnum::Number(b_num) = b.value {
                // Calculate the result
                let res = num_op(a_num, b_num);
                match res {
                    Ok((res_num, bl)) => Ok((define_value! {b.location, modifier!{None}, content!{res_num => number}}, bl)),
                    Err(e) => Err(e) // Pass error onwards
                }
            } else {
                Err("second argument not a number".to_owned())
            }
        } else {
            Err("first argument not a number".to_owned())
        }
    })
}

/// A variant of variadic reduce, with a boolean flag. Depending on the setting, a single TRUE or FALSE may short-circuit a function, and return it immediately. As such, the return type is also always a boolean value
/// Two behaviors are possible: and-like, where any FALSE will result in FALSE, and or-like, where any TRUE will result in TRUE
///
/// # Nonstandard arguments
/// * `fun_name` - Function name to show in error messages
/// * `operator` - Boxed closure, taking two values and returning a result plus a short-circuit flag
/// * `is_and` - If TRUE, use AND short-circuit behavior, otherwise OR
pub fn variadic_reduce_bool_fn(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, fun_name: &str, operator: Box<Fn(types::JValue, types::JValue) -> Result<(types::JValue, bool), String>>, is_and: bool) -> types::JIEvalResult {
    // Start as usual, attempt to pull in the first parameter
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        // Pull in the initial starting value
        let initial_val_unevaled = nonempty.head.clone();
        let initial_val : types::JValue;
        evaluate_or_fail!(initial_val_unevaled in state as initial_val);

        // Next, attempt to iterate through the rest of the arguments
        let mut result : Option<types::JValue> = None;
        let mut current_node : Rc<RefCell<types::JVListNode>> = nonempty.tail.clone();

        while let types::JVListNode::Nonempty(inner_nonempty) = &*current_node.clone().borrow() {
            // Extract and evaluate parameter
            let next_val_unevaled = inner_nonempty.head.clone();
            let next_val : types::JValue;

            evaluate_or_fail!(next_val_unevaled in state as next_val);
            // Call our function
            let res = operator(result.unwrap_or(initial_val.clone()), next_val);

            match res {
                Err(e) => {return types::JIEvalResult::Stop(format!("{}: {}", fun_name, e));}
                Ok((val, stop_bool)) => {
                    // Here, this diverges from the classic variadic reduce
                    if stop_bool == !is_and {
                        // If we get a boolean value that is an inverse of the stop condition (AND requires all are true => FALSE stops, OR requires one is TRUE => TRUE stops), return that
                        return types::JIEvalResult::Return(generate_bool_value(&mut state.current_scope.clone(), !is_and));
                    }
                    result = Some(val);
                    current_node = inner_nonempty.tail.clone();
                }
            }
        }

        match result {
            Some(_) => types::JIEvalResult::Return(generate_bool_value(&mut state.current_scope.clone(), is_and)), // If we get this far, assume that things have ran their course and we need to return the appropriate value
            None => types::JIEvalResult::Stop(format!("{}: needs at least 2 arguments, got 1", fun_name))
        }
    } else {
        types::JIEvalResult::Stop(format!("{}: needs at least 2 arguments, got 0", fun_name))
    }
}

/// Numeric plus/sum
pub fn numeric_plus(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_fn(state, params, "plus", variadic_numeric_wrapper(Box::new(|a,b| Ok(a.wrapping_add(b)))))
}

/// Numeric minus/subtraction
pub fn numeric_minus(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_fn(state, params, "minus", variadic_numeric_wrapper(Box::new(|a,b| Ok(a.wrapping_sub(b)))))
}

/// Numeric multiplication
pub fn numeric_mul(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_fn(state, params, "mul", variadic_numeric_wrapper(Box::new(|a,b| Ok(a.wrapping_mul(b)))))
}

/// Numeric division, erroring if attempting to divide by zero
pub fn numeric_div(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_reduce_fn(state, params, "div", variadic_numeric_wrapper(Box::new(|a,b| {
        if b == 0 {
            Err("divide by zero".to_owned())
        } else {
            Ok(a.wrapping_div(b))
        }
    })))
}

/// Variadic numeric function wrapper; adds matching guards to number-only functions to assure type safety
/// Should not be confused with the reducing boolean variant, this solely reduces two numbers to one with no special short-circuit properties
pub fn variadic_numeric_wrapper(num_op: Box<Fn(types::JVNumber, types::JVNumber) -> Result<types::JVNumber, String>>) -> Box<Fn(types::JValue, types::JValue) -> Result<types::JValue, String>> {
    Box::new(move |a: types::JValue, b: types::JValue| -> Result<types::JValue, String> {
        if let types::JValueStorageEnum::Number(a_num) = a.value {
            if let types::JValueStorageEnum::Number(b_num) = b.value {
                // Calculate the result
                let res = num_op(a_num, b_num);
                match res {
                    Ok(res_num) => Ok(define_value! {b.location, modifier!{None}, content!{res_num => number}}),
                    Err(e) => Err(e) // Pass error onwards
                }
                // define_value! {default_code_location!(), modifier!{None}, content!{function_def => function}}
            } else {
                Err("second argument not a number".to_owned())
            }
        } else {
            Err("first argument not a number".to_owned())
        }
    })
}

/// General variadic reducing function, that does not require further context to combine. Several common operations are very variadic in their nature, so it makes sense to combine their scaffold into a common function
///
/// # Nonstandard arguments
/// * `fun_name` - Function name to show in error messages
/// * `operator` - Boxed closure, taking two values and returning a result plus a short-circuit flag
pub fn variadic_reduce_fn(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, fun_name: &str, operator: Box<Fn(types::JValue, types::JValue) -> Result<types::JValue, String>>) -> types::JIEvalResult {
    // Start as usual, attempt to pull in the first parameter
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        // Pull in the initial starting value
        let initial_val_unevaled = nonempty.head.clone();
        let initial_val : types::JValue;
        evaluate_or_fail!(initial_val_unevaled in state as initial_val);

        // Next, attempt to iterate through the rest of the arguments
        let mut result : Option<types::JValue> = None;
        let mut current_node : Rc<RefCell<types::JVListNode>> = nonempty.tail.clone();

        while let types::JVListNode::Nonempty(inner_nonempty) = &*current_node.clone().borrow() {
            // Extract and evaluate parameter
            let next_val_unevaled = inner_nonempty.head.clone();
            let next_val : types::JValue;

            evaluate_or_fail!(next_val_unevaled in state as next_val);
            // Call our function
            let res = operator(result.unwrap_or(initial_val.clone()), next_val);

            match res {
                Err(e) => {return types::JIEvalResult::Stop(format!("{}: {}", fun_name, e));}
                Ok(val) => {
                    result = Some(val);
                    current_node = inner_nonempty.tail.clone();
                }
            }
        }

        match result {
            Some(val) => types::JIEvalResult::Return(val),
            None => types::JIEvalResult::Stop(format!("{}: needs at least 2 arguments, got 1", fun_name))
        }
    } else {
        types::JIEvalResult::Stop(format!("{}: needs at least 2 arguments, got 0", fun_name))
    }
}

/// String concatenation
pub fn str_concat(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_scoped_reduce_fn(state, params, "<>", Box::new(|scope, a, b| {
        let str_a = stringify(scope, &a, StringificationType::Normal);
        let str_b = stringify(scope, &b, StringificationType::Normal);

        let concatenated_str = str_a + &str_b;

        Ok(define_value! {b.location, modifier!{None}, content!{concatenated_str => string}})
    }))
}

/// General variadic reducing function, that provides context in addition to values, in form of a scope reference
///
/// # Nonstandard arguments
/// * `fun_name` - Function name to show in error messages
/// * `operator` - Boxed closure, taking two values and returning a result plus a short-circuit flag
pub fn variadic_scoped_reduce_fn(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, fun_name: &str, mut operator: Box<FnMut(&mut Rc<RefCell<types::JScope>>, types::JValue, types::JValue) -> Result<types::JValue, String>>) -> types::JIEvalResult {
    // Start as usual, attempt to pull in the first parameter
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        // Pull in the initial starting value
        let initial_val_unevaled = nonempty.head.clone();
        let initial_val : types::JValue;
        evaluate_or_fail!(initial_val_unevaled in state as initial_val);

        // Next, attempt to iterate through the rest of the arguments
        let mut result : Option<types::JValue> = None;
        let mut current_node : Rc<RefCell<types::JVListNode>> = nonempty.tail.clone();

        while let types::JVListNode::Nonempty(inner_nonempty) = &*current_node.clone().borrow() {
            // Extract and evaluate parameter
            let next_val_unevaled = inner_nonempty.head.clone();
            let next_val : types::JValue;

            evaluate_or_fail!(next_val_unevaled in state as next_val);
            // Call our function
            let res = operator(&mut state.current_scope.clone(), result.unwrap_or(initial_val.clone()), next_val);

            match res {
                Err(e) => {return types::JIEvalResult::Stop(format!("{}: {}", fun_name, e));}
                Ok(val) => {
                    result = Some(val);
                    current_node = inner_nonempty.tail.clone();
                }
            }
        }

        match result {
            Some(val) => types::JIEvalResult::Return(val),
            None => types::JIEvalResult::Stop(format!("{}: needs at least 2 arguments, got 1", fun_name))
        }
    } else {
        types::JIEvalResult::Stop(format!("{}: needs at least 2 arguments, got 0", fun_name))
    }
}

/// Implementation of function definitions; parse arguments, and create an appropriate function object
pub fn func_inner(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, last_vararg: bool, scope_type: types::JScopeRef) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        let argument_list_unevaled = nonempty.head.clone();
        let argument_list;
        evaluate_or_fail!(argument_list_unevaled in state as argument_list);
        let mut argument_vector : Vec<types::JVSymbolIdentifier> = Vec::new();
        // Validate that our argument specifications are valid symbols
        match &argument_list.value {
            types::JValueStorageEnum::List(list_node) => {
                let mut current_node = list_node.clone();

                    while *current_node.borrow() != types::JVListNode::Empty { // Dereference pointer, and borrow
                        if let types::JVListNode::Nonempty(inner_nonempty) = &*current_node.clone().borrow() {
                            match inner_nonempty.head.value {
                                types::JValueStorageEnum::Symbol(sym_id) => {
                                    if state.current_scope.borrow().world.borrow().name_for_id(&sym_id).unwrap().starts_with("#") {
                                        return types::JIEvalResult::Stop("func: function parameters may not start with a #".to_owned());
                                    }

                                    // Otherwise, this is OK. Add to our arguments
                                    if argument_vector.contains(&sym_id) {
                                        return types::JIEvalResult::Stop("func: duplicate argument identifiers".to_owned());
                                    }
                                    argument_vector.push(sym_id);
                                    current_node = inner_nonempty.tail.clone();
                                }
                                _ => {return types::JIEvalResult::Stop("func: not a symbol".to_owned());}
                            }
                        } else {
                            // We have refuted this in the outer while
                            unreachable!()
                        }
                    }


                if last_vararg == true && argument_vector.len() < 1 {
                    return types::JIEvalResult::Stop("func: vararg functions must have at least one formal parameter".to_owned());
                }
            }
            _ => {return types::JIEvalResult::Stop("func: first argument must be a list of symbols".to_owned());}
        }


        if let types::JVListNode::Nonempty(tail_nonempty) = &*nonempty.tail.borrow() { // This I'm not quite sure about.. apparently, compiler is unable to recognize a safe situation without clone()
            // Finally, return a function
            let value_unevaled = tail_nonempty.head.clone();
            let value_location = value_unevaled.location.clone();
            let value;
            evaluate_or_fail!(value_unevaled in state as value);
            let function_def = types::JFunction {last_param_vararg: last_vararg, param_symbols: argument_vector, interpret_in_scope: scope_type, code: Rc::new(RefCell::new(value))};
            return types::JIEvalResult::Return(define_value! {value_location, modifier!{None}, content!{function_def => function}});
        } else {
            return types::JIEvalResult::Stop("lambda: needs at least 2 arguments to create a function, found 1".to_owned());
        }

        // Nothing more to do, return one of the above
    } else {
        types::JIEvalResult::Stop("func: needs at least 2 arguments to create a function".to_owned())
    }
}

/// Simple functions; essentially, bits of code declared with appropriate formal parameters and arity
pub fn func(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    func_inner(state, params, false, types::JScopeRef::Implicit)
}

/// Simple, vararg functions
pub fn func_vararg(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    func_inner(state, params, true, types::JScopeRef::Implicit)
}

/// Fixed arity stdlib functions
pub fn func_stdlib(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    func_inner(state, params, false, types::JScopeRef::ImplicitStdlib)
}

/// Vararg stdlib functions
pub fn func_stdlib_vararg(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    func_inner(state, params, true, types::JScopeRef::ImplicitStdlib)
}

/// Normal cons, evaluates both head and tail
pub fn cons(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    cons_inner(state, params, true)
}

/// No-head-eval cons (tail evaluated, head added verbatim), intended for essentially special purposes
pub fn cons_no_head_eval(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    cons_inner(state, params, false)
}

/// Implementation of simple cons. Concatenate two parameters into a list, and stop if it is impossible to do. Optionally does not evaluate head
///
/// # Nonstandard arguments
/// * `eval_head` - if true, evaluate head instead of adding it verbatim
pub fn cons_inner(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, eval_head: bool) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        // Pull in the head
        let head_raw = nonempty.head.clone();
        let mut head;
        if eval_head {
            // Evaluate the head normally
            evaluate_or_fail!(head_raw in state as head);
        } else {
            head = head_raw.clone()
        }

        if let types::JVListNode::Nonempty(inner_nonempty) = &*(nonempty.tail).borrow() {
            // Check if we have too many arguments?
            if let types::JVListNode::Nonempty(_) = &*(inner_nonempty.tail).borrow() {
                return types::JIEvalResult::Stop("cons: Too many arguments".to_owned());
            }
            let tail_raw = inner_nonempty.head.clone();
            let mut tail;
            evaluate_or_fail!(tail_raw in state as tail);

            // Check if this tail is of correct form?
            match tail.value {

                types::JValueStorageEnum::List(list_node) => {
                    let head_location = head.location.clone();
                    let cons_list_node = nonempty_list_node!(head, list_node.clone());

                    return types::JIEvalResult::Return(define_value! {head_location, modifier!{None}, content!{cons_list_node => list}});
                }
                _ => types::JIEvalResult::Stop("cons: Tail is not a list".to_owned()),
            }
        } else {
            types::JIEvalResult::Stop("cons: Needs 2 arguments to concatenate a list, found 1".to_owned())
        }
    } else {
        types::JIEvalResult::Stop("cons: Needs 2 arguments to concatenate a list, found 0".to_owned())
    }
}

/// Simple readline; reads a line from the standard input, and returns it as a string. Input is trimmed to remove extra whitespace and newlines
pub fn readline(_state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    if *(params).borrow() != types::JVListNode::Empty {
        return types::JIEvalResult::Stop("readln: no arguments should be provided".to_owned());
    }

    // As almost verbatim from samples for Rust
    let mut input = String::new();

    match io::stdin().read_line(&mut input) {
        Ok(_) => { // Line length not required here
            let trimmed_str = input.trim_matches(char::is_whitespace).to_owned();
            types::JIEvalResult::Return(define_value!{types::JLocationData {file: "stdin".to_owned(), pos: 0}, modifier!{None}, content!{trimmed_str => string}})
        }
        Err(error) => types::JIEvalResult::Stop(format!("readline: I/O error: {}", error))
    }
}

/// Generate a boolean value
pub fn generate_bool_value(scope: &mut Rc<RefCell<types::JScope>>, b: bool) -> types::JValue {
    let sym_name = if b {"#t"} else {"#f"};
    let symbol = define_symbol!(sym_name => (scope.borrow_mut().world)).unwrap();

    return define_value!{default_code_location!(), modifier!{None}, content!{symbol => symbol}}
}

/// Is a value truthy?
/// Truthiness is defined as being anything else but a zero, a #f or a #undef
fn is_truthy(scope: &mut Rc<RefCell<types::JScope>>, value: &types::JValue) -> bool {
    //println!("{:?}", value);
    match value.value {
        types::JValueStorageEnum::Number(n) => n != 0,
        types::JValueStorageEnum::Symbol(sym_id) => {
            let symbol_undef = define_symbol!("#undef" => (scope.borrow_mut().world)).unwrap();
            let symbol_falsy = define_symbol!("#f" => (scope.borrow_mut().world)).unwrap();
            !(symbol_falsy == sym_id || symbol_undef == sym_id)
        }
        _ => true // Anything else is true
    }
}

/// OR, as defined with a variadic function
pub fn truthy_or(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_scoped_bool(state, params, "or", Box::new(move |scope, val| {Ok(is_truthy(scope, &val))}), false)
}

/// AND, as defined with a variadic function
pub fn truthy_and(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    variadic_scoped_bool(state, params, "and", Box::new(move |scope, val| {Ok(is_truthy(scope, &val))}), true)
}

/// Variadic, scope-dependent boolean functions. Essentially, require that all are true or one is true according to a helper function. Invocations with no parameters return a false
/// # Nonstandard arguments
/// * `fun_name` - Function name to show in error messages
/// * `operator` - Boxed closure, taking two values and returning a result plus a short-circuit flag
pub fn variadic_scoped_bool(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, fun_name: &str, mut op: Box<FnMut(&mut Rc<RefCell<types::JScope>>, types::JValue) -> Result<bool, String>>, is_and: bool) -> types::JIEvalResult {
    // Start as usual, attempt to pull in the first parameter
    let mut current_node = params.clone();
    let mut found_true = false;

    while let types::JVListNode::Nonempty(inner_nonempty) = &*current_node.clone().borrow() {
        // Extract and evaluate parameter
        let next_val_unevaled = inner_nonempty.head.clone();
        let next_val : types::JValue;

        evaluate_or_fail!(next_val_unevaled in state as next_val);

        // Call our function, and determine the result
        found_true = match op(&mut state.current_scope.clone(), next_val) {
            Ok(b) => b,
            Err(e) => {
                return types::JIEvalResult::Stop(format!("{}: {}", fun_name, e));
            }
        };

        // If we found a true and we want an OR, exit. If we got a false and we want an AND, exit
        if found_true != is_and { break; } else {
            current_node = inner_nonempty.tail.clone();
        }
    }

    return types::JIEvalResult::Return(generate_bool_value(&mut state.current_scope.clone(), found_true))
}

/// Cond, in form of (#cond (clause expr) (clause expr) ...)
pub fn cond(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    // Pre-define #undef
    let symbol_undef = define_symbol!("#undef" => (state.current_scope.clone().borrow_mut().world)).unwrap();
    // Do we have a nonempty parameter list?
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        let head_location = nonempty.head.location.clone();
        let mut cond_clause_expr_base = nonempty.clone(); // Clone, as we need to traverse

        // We are now processing params, one by one
        loop {
            // Attempt to match on the head of the current node
            match &cond_clause_expr_base.clone().head.value {
                // A sublist?
                types::JValueStorageEnum::List(cond_clause_inner_lst) => {
                    // A nonempty sublist?
                    if let types::JVListNode::Nonempty(inner_nonempty) = &*(cond_clause_inner_lst).borrow() {
                        // Evaluate expression contained as the head
                        let mut evaled_cond;
                        let cond = inner_nonempty.head.clone();
                        evaluate_or_fail!(cond in state as evaled_cond);

                        // Is it truthy?
                        if is_truthy(&mut state.current_scope.clone(), &evaled_cond) {
                            // Good! We should now be able to take the tail
                            if let types::JVListNode::Nonempty(cond_expr_nonempty) = &*(inner_nonempty.tail).borrow() {
                                return types::JIEvalResult::Reeval(cond_expr_nonempty.head.clone());  // Return the expression
                            } else {
                                return types::JIEvalResult::Stop("cond: no subsequent expression to evaluate after a successful condition provided".to_owned());
                            }
                        } else {
                            false // No match, next
                        }
                    } else {
                        return types::JIEvalResult::Stop("cond: condition-expr must be nonempty".to_owned());
                    }
                },
                _ => {return types::JIEvalResult::Stop("cond: condition-expr must be a list".to_owned()); }
            };

            // Check if we can traverse to a next list
            if let types::JVListNode::Nonempty(next_cond_clause_expr_nonempty) = &*(cond_clause_expr_base.clone().tail).borrow() {
                cond_clause_expr_base = next_cond_clause_expr_nonempty.clone();
                continue;
            }

            return types::JIEvalResult::Return(define_value! {head_location, modifier!{None}, content!{symbol_undef => symbol}});
        }

    } else {
        // Return an empty list
        types::JIEvalResult::Return(define_value! {default_code_location!(), modifier!{None}, content!{symbol_undef => symbol}})
    }
}

/// Head of a list
pub fn list_head(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    list_head_tail(state, params, true)
}

/// Tail or a list
pub fn list_tail(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    list_head_tail(state, params, false)
}

/// Helper function, return the head or tail of a list, or stop otherwise
/// # Nonstandard arguments
/// * `return_head` - True if head should be returned, tail otherwise
pub fn list_head_tail(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>, return_head: bool) -> types::JIEvalResult {
    if let types::JVListNode::Nonempty(nonempty) = &*params.borrow() {
        // Check that we do not have excess arguments
        if *(nonempty.tail).borrow() != types::JVListNode::Empty {
            types::JIEvalResult::Stop("list_headtail: too many arguments".to_owned());
        }

        // Evaluate first and only argument
        let head_raw = nonempty.head.clone();
        // In this case, it makes sense to set the location at the invocation, as to aid debugging

        let head_raw_location = head_raw.location.clone();
        let mut head : types::JValue;

        evaluate_or_fail!(head_raw in state as head);

        // Attempt to match it as a list, and return its head

        match head.value {
            types::JValueStorageEnum::List(list_node) => {
                match &*list_node.borrow() {
                    types::JVListNode::Nonempty(inner_nonempty) => {
                        let mut ret_value = if return_head {
                            inner_nonempty.head.clone()
                        } else {
                            define_value! {default_code_location!(), modifier!{None}, content!{inner_nonempty.tail.clone() => list}}
                        };

                        ret_value.location = head_raw_location;
                        types::JIEvalResult::Return(ret_value)
                    }
                    types::JVListNode::Empty => {
                        types::JIEvalResult::Stop("list_headtail: argument is an empty list".to_owned())
                    }
                }
            }
            _ => types::JIEvalResult::Stop("list_headtail: argument not a list".to_owned())
        }

    } else {
        types::JIEvalResult::Stop("list_headtail: needs 1 argument, found 0".to_owned())
    }
}

/// Simple list evaluation, evaluate all list arguments and build a new list
pub fn list_eval(state: &types::JIState, params: &Rc<RefCell<types::JVListNode>>) -> types::JIEvalResult {
    let mut list_location = default_code_location!(); // Default, emtpy location

    let mut evaled_list_head = empty_list_node!(); // What is the head of our list?
    let mut evaled_list_current = evaled_list_head.clone();

    let mut pending_list_current = params.clone();

    // As long as we have a tail, iterate and construct a new list with evaluated arguments
    loop {
        let mut next_head;

        if let types::JVListNode::Nonempty(nonempty_head) = &*(pending_list_current).borrow() {
            let head_expr_result = core::evaluate_with_state(state, nonempty_head.head.clone());

            match head_expr_result {
                Err(e) => { return types::JIEvalResult::Stop(e); }
                Ok(jv) => {
                    // Create a new list node for the current element
                    let new_node = nonempty_list_node!(jv, empty_list_node!());

                    if evaled_list_head == empty_list_node!() {
                        // First parameter, assign to its raw location as this points to the invocation
                        list_location = nonempty_head.clone().head.location.clone();
                        evaled_list_head = new_node.clone();
                        evaled_list_current = new_node.clone(); // Replace head with instances of this
                    } else {
                        {
                            let node_ref = &mut *evaled_list_current.borrow_mut();
                            match node_ref {
                                types::JVListNode::Nonempty(lst) => {
                                    lst.tail = new_node.clone();
                                }
                                _ => { unreachable!() }
                            }
                        }
                        evaled_list_current = new_node.clone();
                    }
                    ;
                }
            }

            next_head = nonempty_head.tail.clone();
        } else {
            break;
        }

        pending_list_current = next_head;
    }
    types::JIEvalResult::Return(define_value! {list_location, modifier!{None}, content!{evaled_list_head => list}})
}