/**
    Juoni - a Scheme-like toy language written in Rust
    --
    Copyright 2019 Arttu Ylä-Sahra

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
    world_ops.rs - World operations - initializing a new world, handling symbols, et al
**/

use super::types;
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;
use super::parser;

/// Initializes a new, entirely blank interpreter world
pub fn initialize_world() -> Rc<RefCell<types::JInterpreterWorld>> {
    Rc::new(RefCell::new(types::JInterpreterWorld {
        symbol_map: HashMap::new(),
        inverse_symbol_map: HashMap::new(),
        symbol_counter: 0
    }))
}

impl types::JInterpreterWorld {

    /// When given a string, either returns a symbol identifier, or nothing if the symbol name is invalid. Panics if the symbol table is too full
    /// # Arguments
    /// `name` - Reference to a string denoting the symbol name
    pub fn symbol_id_for_name(&mut self, name: &str) -> Option<types::JVSymbolIdentifier> {
        if !valid_symbol_name(name) {
            // Not a valid name, return None
            None
        } else {
            // Check first if we already know an identifier for this name.
            let symbol_lookup = self.symbol_map.get(name);
            if symbol_lookup == None {
                // Store a new symbol identifier
                self.symbol_map.insert(name.to_owned(), self.symbol_counter);
                self.inverse_symbol_map.insert(self.symbol_counter, name.to_owned());
                // Advance the symbol counter, and compensate for it when returning
                self.symbol_counter += 1;
                Some(self.symbol_counter-1)
            } else {
                Some(symbol_lookup.unwrap().clone())
            }
        }
    }

    /// Determines if a name exists for a given symbol ID
    /// # Arguments
    /// `id` - Symbol ID
    pub fn name_for_id(&self, id: &types::JVSymbolIdentifier) -> Option<types::JVString> {
        let res = self.inverse_symbol_map.get(id);
        match res {
            None => None,
            Some(strref) => Some(strref.to_owned())
        }
    }
}

/// Checks if this symbol name is grammatically valid - it does not account for semantic validity!
/// This is effectively a proxy to parser module implementing this functionality, but remains here due to past design decisions
///
pub fn valid_symbol_name(name: &str) -> bool {
    parser::is_valid_symbol(name)
}