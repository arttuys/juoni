/**
    Juoni - a Scheme-like toy language written in Rust
    --
    Copyright 2019 Arttu Ylä-Sahra

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
    types.rs - Core Juoni datatypes
**/

use std::collections::HashMap;
use std::rc::Rc;
use std::rc::Weak;
use std::vec::Vec;
use std::cell::RefCell;
use std::fmt;
////////////////// PRIMITIVE TYPES

/// Symbol identifiers are somewhat similar to location identifiers, but smaller - we don't need nearly as many symbols
/// One should note that symbol identifiers are _global_ thorough the entire environment!
pub type JVSymbolIdentifier = u32;

/// As specified, Juoni numbers are signed 64-bit
pub type JVNumber = i64;

///////////////// COMPLEX TYPES

/// Using primitive &str slices would be messy to implement in terms of lifetime qualifications and whatnot, so the standard mutable String type is used
pub type JVString = String;

/// List node; is either an empty list or a nonempty node with a head and a tail
#[derive(Clone, Debug, PartialEq)]
pub enum JVListNode {
    Empty,
    Nonempty(JVListNonempty)
}

/// Simple display implementation for Juoni lists
impl fmt::Display for JVListNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            crate::interpreter::types::JVListNode::Empty => "empty list node".to_owned(),
            crate::interpreter::types::JVListNode::Nonempty(ref inner_lst_ref) => {
                format!("nonempty list node, containing {}", &inner_lst_ref)
            }
        };
        write!(f, "{}", text)
    }
}

/// Nonempty list: contains a value of some nature, and a pointer to the next list
#[derive(Clone, Debug, PartialEq)]
pub struct JVListNonempty {
    pub head: JValue,
    pub tail: Rc<RefCell<JVListNode>>
}

/// Display for nonempty list nodes
impl fmt::Display for JVListNonempty {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tail_str = match self.tail.try_borrow() {
            Err(_) => "<?>".to_owned(),
            Ok(borrow) => {format!("{}", *borrow)}
        };
        write!(f, "head: {}, tail: ({})", self.head, tail_str)
    }
}

/////////////// VALUE AND STORAGE

/// A JValue is the uppermost value type. It contains modifier information (as filled in by the parser), location data (where did this value originate from?) and the value itself
#[derive(Clone, Debug)]
pub struct JValue {
    pub modifier: JVModifier,
    pub location: JLocationData,
    pub value: JValueStorageEnum
}

/// As locations are auxillary information, it makes little sense for them to be considered for equivalence.
/// So, two JValues are equivalent iff their modifiers match, and their inner values are equivalent
impl PartialEq for JValue {
    fn eq(&self, other: &JValue) -> bool {
        self.value == other.value && self.modifier == other.modifier
    }
}

/// Simple display for JValues
impl fmt::Display for JValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(value at {}, {}, {})", self.location, self.modifier, self.value)
    }
}

/// As values can have several distinct types, it is beneficial to strictly separate them with an enum type
#[derive(Clone, Debug, PartialEq)]
pub enum JValueStorageEnum {
    Number(JVNumber),
    Symbol(JVSymbolIdentifier),
    String(JVString),
    List(Rc<RefCell<JVListNode>>), // Consider that lists may have references and may be referenced to from multiple sources, a Rc<RefCell<>> is rather necessary
    SpecialForm(JSpecialForm),
    Function(JFunction) // Note: be sure to read the caveat of function equality
}

/// Display for JValueStorageEnum
impl fmt::Display for JValueStorageEnum {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            crate::interpreter::types::JValueStorageEnum::Number(n) => {
                write!(f, "number '{}'", n)
            }
            crate::interpreter::types::JValueStorageEnum::Symbol(id) => {
                write!(f, "symbol ID {}", id)
            }
            crate::interpreter::types::JValueStorageEnum::String(s) => {
                write!(f, "string '{}'", s)
            }
            crate::interpreter::types::JValueStorageEnum::List(lst) => {
                // This is a bit more fiddly. We need to attempt a borrow, which _may_ fail!
                let inner_list_print = match lst.try_borrow() {
                    Err(_) => "<?>".to_owned(),
                    Ok(borrow) => {
                        format!("{}", *borrow)
                    }
                };

                write!(f, "list of type: {}", inner_list_print)
            }
            crate::interpreter::types::JValueStorageEnum::SpecialForm(sf) => {write!(f, "special form '{}'", sf.name.clone())}
            crate::interpreter::types::JValueStorageEnum::Function(ff) => {
                let inner_code_print = match ff.code.try_borrow() {
                    Err(_) => "<?>".to_owned(),
                    Ok(borrow) => {
                        format!("{}", *borrow)
                    }
                };
                write!(f, "function with declared symbols for params {:?}, last vararg: {}, defined as: '{}'", ff.param_symbols , ff.last_param_vararg , inner_code_print)
            }
        }
    }
}

/// For convenience and simplifying design purposes, modifiers are stored as parts of values
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum JVModifier {
    None,
    Verbatim,
    Quote,
    Unquote,
    UnquoteDestructure
}

/// Simple modifier display
impl fmt::Display for JVModifier {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let modifier_expl = match self {
            crate::interpreter::types::JVModifier::None => "no modifiers",
            crate::interpreter::types::JVModifier::Verbatim => "verbatim",
            crate::interpreter::types::JVModifier::Quote => "quoted",
            crate::interpreter::types::JVModifier::Unquote => "unquoted",
            crate::interpreter::types::JVModifier::UnquoteDestructure => "unquoted with destructuring"
        };

        write!(f, "{}", modifier_expl)
    }
}

/// Location data structure, with filename/other indicator of origin and position
#[derive(Clone, Debug, PartialEq)]
pub struct JLocationData {
    pub file: String,
    pub pos: usize
}

/// Simple display for position
impl fmt::Display for JLocationData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<{}:{}>", self.file, self.pos)
    }
}

/// Juoni functions, defined in Juoni language as compared to special forms which are native Rust functions
#[derive(Clone, Debug)]
pub struct JFunction {
    pub param_symbols: Vec<JVSymbolIdentifier>, // With which symbols we bind our parameters?
    pub last_param_vararg: bool, // Do we bind varargs to last formal parameter?
    pub code: Rc<RefCell<JValue>>, // We are likely to run into scenarios where we have two slightly different, but essentially equivalent definitions of the same function.
    pub interpret_in_scope: JScopeRef // How we handle scope?
}

/// This is an important observation - two functions _can be_ considered equivalent, _even if_ they have differing "parent" scopes.
/// That is partially due to technical reasons - if we added scopes in our comparison, we could end up in a situation where we are not able
/// to reliably compare functions due to Rust borrows prohibiting so. It is also possible that we have two functions that are effectively the same, even if they have two nominally different scopes.
/// So, for the scope of functions, they are considered equivalent iff: both have same arity, same symbols, and same code to evaluate
impl PartialEq for JFunction {
    fn eq(&self, other: &JFunction) -> bool {
        self.param_symbols == other.param_symbols && self.last_param_vararg == other.last_param_vararg && self.code == other.code
    }
}

/// This is very useful for avoiding problematic situations. For a given function, you can either give it an implicit scope, in which case it inherits from the surrounding environment, or alternatively define a definite scope, which is a strong binding
#[derive(Clone, Debug)]
pub enum JScopeRef {
    Implicit, // Implicit parent to enclosing scope
    ImplicitStdlib, // Implicit parent to standard library
    FunctionParentScope(Weak<RefCell<JScope>>) // Implicit relation to function parent - this allows safe self-recursion without stack growth, as we know the immediate parent scope
}

/// Special forms have a human-readable name, and a special function pointer that is a native function
/// This is, effectively, the standard escape gateway from evaluated code to functionality that cannot be implemented in any other way than native code
#[derive(Clone)]
pub struct JSpecialForm {
    pub name: String,
    pub pointer: Rc<RefCell<JSFPointer>> // Pointer to a function that takes a state, a tail list of parameters (unevaluated), and returns a value
}

/// A special form takes a set of parameters, reference to a state, and returns an interpretation result
pub type JSFPointer = Fn(&JIState, &Rc<RefCell<JVListNode>>) -> JIEvalResult;

/// As function pointers are not naturally comparable, treat pointers with the same name as equivalent.
/// Special forms are not instantiable by a common user, so, it is safe to assume that the programmer has taken care to not create several special forms with equal names but different behavior
impl PartialEq for JSpecialForm {
    fn eq(&self, other: &JSpecialForm) -> bool {
        self.name == other.name
    }
}

/// Simple debug behavior for special forms
impl fmt::Debug for JSpecialForm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<special form, name '{}'>", self.name)
    }
}

///////////////////// INTERPRETER STATE

/// For the needs of various special forms and general functionality, we need to keep track of various auxiliary properties
/// This is done with a state object
#[derive(Clone, Debug)]
pub struct JIState {
    pub current_scope: Rc<RefCell<JScope>>,
    pub stdlib_scope: Rc<RefCell<JScope>>
}

/// Evaluation result, signifying how an inner evaluation of an expression turned out.
#[derive(Clone, Debug)]
pub enum JIEvalResult {
    Return(JValue), // Expression was completely evaluated, and this result should be returned as is.
    Reeval(JValue), // The returned expression should be evaluated at same level. This avoids growing stack for certain simple cases, like if and cond
    ReevalNewScope(JValue, Rc<RefCell<JScope>>), // Returned expression should be evaluated, after current scope has been changed. Needed for function calls
    Stop(String) // An interpreter error occurred, stop.
}

////////////// ENVIRONMENT AND SCOPE

/// The shared "world", containing the symbol map, inverse map, and counter
#[derive(Debug, PartialEq, Clone)]
pub struct JInterpreterWorld {
    pub symbol_map: JSymbolMap,
    pub inverse_symbol_map: JInverseSymbolMap,
    pub symbol_counter: JVSymbolIdentifier
}

/// Symbols are indexed by their textual representation, and vis a versa
pub type JSymbolMap = HashMap<JVString, JVSymbolIdentifier>;
pub type JInverseSymbolMap = HashMap<JVSymbolIdentifier, JVString>;

/// A variable binding map creates a relation from a given symbol to a value. These can also capture modifiers
pub type JVariableBindingMap = HashMap<JVSymbolIdentifier, JValue>;

/// A scope, which contains a mutable reference (note: checked at runtime due to Rust borrowing rules!) to the common world, a variable map, and a possible optional reference to a parent scope
#[derive(Clone, Debug, PartialEq)]
pub struct JScope {
    pub world: Rc<RefCell<JInterpreterWorld>>,
    pub variable_bindings: JVariableBindingMap,
    pub parent_scope: Option<Rc<RefCell<JScope>>>,
    pub rand_id: String
}